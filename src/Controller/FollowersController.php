<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

/**
 * Followers Controller
 *
 * @property \App\Model\Table\FollowersTable $Followers
 *
 * @method \App\Model\Entity\Follower[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersController extends AppController
{
    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // The add and routes actions are always allowed to logged in users.
        if (in_array($action, ['follow', 'followers', 'followings'])) {
            return true;
        }

        // All other actions require a id.
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }
        try {
            // Check that the user belongs to the current user.
            $follower = $this->Followers->get($id);

            // boolean result
            return $user['id'] === $follower['user_id'];
        } catch (\Throwable $exception) {
            return false;
        }
    }

    /**
     * Action for follow
     */
    public function follow()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $entity = $this->request->getData();
            $follow = $this->Followers->newEntity();
            $value = ['status' => null];

            //check if user exists
            $user = TableRegistry::getTableLocator()->get('Users');
            if (!($user->exists(['id' => $entity['followed_user_id']])) ||
                ((int)$entity['followed_user_id'] === (int)$this->Auth->user('id'))) {
                $value = ['status' => 'failed'];
            } else {
                if ($this->Followers->exists([
                    'user_id' => (int)$this->Auth->user('id'),
                    'followed_user_id' => $entity['followed_user_id']
                ])) {
                    $follow = $this->Followers
                        ->find()
                        ->where([
                            'Followers.user_id' => (int)$this->Auth->user('id'),
                            'Followers.followed_user_id' => $entity['followed_user_id']
                        ])
                        ->first();
                    ($follow['status'] === true) ? $follow->set('status', false) : $follow->set('status', true);

                    if ($this->Followers->save($follow)) {
                        $value = ['status' => $follow['status']];
                    }
                } else {
                    $entity['user_id'] = (int)$this->Auth->user('id');
                    $entity['status'] = true;
                    $follow = $this->Followers->patchEntity($follow, $entity);
                    if ($this->Followers->save($follow)) {
                        $value = ['status' => $follow['status']];
                    }
                }
                $value['followers'] = $this->Followers
                    ->find()
                    ->where(['Followers.followed_user_id' => $entity['followed_user_id'], 'Followers.status' => true])
                    ->count();
            }
            $referrer = explode('/', $this->referer());

            $refererSlug = end($referrer);
            array_pop($referrer);
            $refererAction = end($referrer);
            if ($refererAction === 'followings' && (int)$refererSlug === (int)$this->Auth->user('id')) {
                $value = ['status' => 'failed'];
            }

            $followers = $value;
            // Set the view vars that have to be serialized.
            $this->set(compact('followers'));

            // Specify which view vars JsonView should serialize.
            $this->set('_serialize', ['followers']);
        }
    }

    /**
     * @param $id
     */
    public function followers($id)
    {

        $followers = $this->paginate($this->Followers
            ->find()
            ->where(['Followers.followed_user_id' => $id, 'Followers.status' => true])
            ->contain(['Users']));
        foreach ($followers as $follower) {
            $follower['followed'] = false;
            if ($this->Followers->exists([
                'user_id' => (int)$this->Auth->user('id'),
                'followed_user_id' => $follower['user']['id'],
                'status' => true
            ])) {
                $follower['followed'] = true;
            }
        }
        $owner = TableRegistry::getTableLocator()->get('Users');
        $owner = $owner->get($id);
        $this->set(compact(['followers', 'owner']));
    }

    /**
     * @param $id
     */
    public function followings($id)
    {

        $followers = $this->paginate($this->Followers
            ->find()
            ->where(['Followers.user_id' => $id, 'Followers.status' => true])
            ->contain(['FollowedUsers'])
            ->order(['Followers.id' => 'ASC']), array(
            'limit' => 6
        ));

        foreach ($followers as $follower) {
            $follower['user'] = $follower['users'];
            unset($follower['followed_users']);
            $follower['followed'] = false;
            if ($this->Followers->exists([
                'user_id' => (int)$this->Auth->user('id'),
                'followed_user_id' => $follower['user']['id'],
                'status' => true
            ])) {
                $follower['followed'] = true;
            }
        }
        $owner = TableRegistry::getTableLocator()->get('Users');
        $owner = $owner->get($id);
        $this->set(compact(['followers', 'owner']));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $followers = $this->paginate($this->Followers);

        $this->set(compact('followers'));
    }

    /**
     * View method
     *
     * @param string|null $id Follower id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $follower = $this->Followers->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('follower', $follower);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $follower = $this->Followers->newEntity();
        if ($this->request->is('post')) {
            $follower = $this->Followers->patchEntity($follower, $this->request->getData());
            if ($this->Followers->save($follower)) {
                $this->Flash->success(__('The follower has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The follower could not be saved. Please, try again.'));
        }
        $users = $this->Followers->Users->find('list', ['limit' => 200]);
        $this->set(compact('follower', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Follower id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $follower = $this->Followers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $follower = $this->Followers->patchEntity($follower, $this->request->getData());
            if ($this->Followers->save($follower)) {
                $this->Flash->success(__('The follower has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The follower could not be saved. Please, try again.'));
        }
        $users = $this->Followers->Users->find('list', ['limit' => 200]);
        $this->set(compact('follower', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Follower id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $follower = $this->Followers->get($id);
        if ($this->Followers->delete($follower)) {
            $this->Flash->success(__('The follower has been deleted.'));
        } else {
            $this->Flash->error(__('The follower could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
