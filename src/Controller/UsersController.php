<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // The add and routes actions are always allowed to logged in users.
        if (in_array($action, ['view', 'search'])) {
            return true;
        }

        // All other actions require a id.
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the user belongs to the current user.
        $owner = $this->Users->get($id);
        // boolean result
        return $user['id'] === $owner['id'];
    }

    /**
     * @throws \Exception
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout', 'register', 'verify']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function register()
    {
        if (!(empty($this->Auth->user('id')))) {
            return $this->redirect(['action' => 'view', $this->Auth->user('id')]);
        }
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user['email_hash'] = Text::uuid();
            if ($this->Users->save($user)) {
                $email = new Email();
                $email->setProfile('gmail');
                $email->setTo($user['email']);
                $email->setSubject('Email Verification');
                $email->setEmailFormat('html');
                $email->setTemplate('email_verification');
                $email->setFrom(array('microblogcatalyst@gmail.com' => 'Microblog'));
                if ($email->send($user['email_hash'])) {
                    $this->Flash->success(__('Well done! Please verify your account through email.'));
                    return $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->error(__('Something went wrong.'));
                }
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function login()
    {
        if (!(empty($this->Auth->user('id')))) {
            return $this->redirect(['action' => 'view', $this->Auth->user('id')]);
        }
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if ($user['status']) {
                    $this->Auth->setUser($user);
                    $this->Flash->success('Welcome back ' . $user['full_name'] . '!');
                    return $this->redirect($this->Auth->redirectUrl());
                }
                $this->Flash->error('Please verify your account through email.');
            } else {
                $this->Flash->error('Your username or password is incorrect.');
            }
        }
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }

    /**
     * @param $emailHash
     * @return \Cake\Http\Response|null
     */
    public function verify($emailHash = null)
    {
        if (!$this->Users->exists(['email_hash' => $emailHash, 'status' => false])) {
            return $this->redirect(['action' => 'login']);
        }

        $user = $this->Users->findByEmailHash($emailHash)->first();
        $user->set('status', true);
        if ($this->Users->save($user)) {
            $this->Auth->setUser($user);
            $this->Flash->success(__('Your account is now verified! Welcome ' . $user['full_name'] . '!'));
            return $this->redirect(['action' => 'view', $user['id']]);
        } else {
            $this->Flash->error(__('Something went wrong.'));
        }

        return $this->redirect(['action' => 'login',]);
    }

    /**
     * @param null $id
     * @return \Cake\Http\Response|null
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id);
        $this->set(compact('user'));

        $followers = TableRegistry::getTableLocator()->get('Followers');

        $follow['status'] = false;
        if ($followers->exists([
            'user_id' => (int)$this->Auth->user('id'),
            'followed_user_id' => $id, 'status' => true
        ])) {
            $follow['status'] = true;
        }
        $follow['followers'] = $followers
            ->find()
            ->where(['Followers.followed_user_id' => $id, 'Followers.status' => true])
            ->count();
        $follow['following'] = $followers
            ->find()
            ->where(['Followers.user_id' => $id, 'Followers.status' => true])
            ->count();
        $this->set(compact('follow'));
        $postEntity = TableRegistry::getTableLocator()->get('Posts');
        $post = $postEntity->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $post = $postEntity->patchEntity($post, $this->request->getData());
            $post['user_id'] = $this->Auth->user('id');
            if ($postEntity->save($post)) {
                $this->Flash->success(__('Posted successfully!'));
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Post failed. Please, try again.'));
        }
        $this->set(compact('post'));
    }

    /**
     * @param null $id
     * @return \Cake\Http\Response|null
     */
    public function update($id = null)
    {
        $user = $this->Users->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Auth->setUser($user);
                $this->Flash->success(__('Information saved!'));
                return $this->redirect(['action' => 'view', $user['id']]);
            }
            $this->Flash->error(__('Information could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * @param null $id
     * @return \Cake\Http\Response|null
     */
    public function changePassword($id = null)
    {
        $user = $this->Users->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Auth->setUser($user);
                $this->Flash->success(__('Password saved!'));
                return $this->redirect(['action' => 'view', $user['id']]);
            }
            $this->Flash->error(__('Password could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     *
     */
    public function search()
    {
        $q = null;
        if ($this->request->is('get')) {
            $q = $this->request->getQuery('q');
        }
        $conditions = array();
        $search_terms = explode(' ', $q);
        foreach ($search_terms as $search_term) {
            $conditions[] = array('Users.username LIKE' => "%$search_term%");
            $conditions[] = array('Users.first_name LIKE' => "%$search_term%");
            $conditions[] = array('Users.last_name LIKE' => "%$search_term%");
            $conditions[] = array('Users.email LIKE' => "%$search_term%");
        }
        $users = $this->paginate($this->Users
            ->find()
            ->order(['Users.first_name' => 'ASC']), array(
            'limit' => 6,
            'conditions' => array(
                'OR' => $conditions
            ),
        ));
        $follow = TableRegistry::getTableLocator()->get('Followers');
        foreach ($users as $user) {
            $user['followed'] = false;
            if ($follow->exists([
                'user_id' => (int)$this->Auth->user('id'),
                'followed_user_id' => $user['id'],
                'status' => true
            ])) {
                $user['followed'] = true;
            }
        }
        $this->set(compact('users'));
    }
}
