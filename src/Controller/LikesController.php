<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\LikesTable $Likes
 *
 * @method \App\Model\Entity\Like[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LikesController extends AppController
{
    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // The add and routes actions are always allowed to logged in users.
        if (in_array($action, ['react'])) {
            return true;
        }

        // All other actions require a id.
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }
        try {
            // Check that the user belongs to the current user.
            $like = $this->Likes->get($id);

            // boolean result
            return $user['id'] === $like['user_id'];
        } catch (\Throwable $exception) {
            return false;
        }
    }

    public function react()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $entity = $this->request->getData();
            $react = $this->Likes->newEntity();
            $value = ['status' => null, 'count' => null];
            if ($this->Likes->exists(['user_id' => (int)$this->Auth->user('id'), 'post_id' => $entity['post_id']])) {
                $react = $this->Likes
                    ->find()
                    ->where(['Likes.user_id' => (int)$this->Auth->user('id'), 'Likes.post_id' => $entity['post_id']])
                    ->first();
                ($react['status'] === true) ? $react->set('status', false) : $react->set('status', true);

                if ($this->Likes->save($react)) {
                    $count = $this->Likes->find('all')->where([
                        'Likes.post_id' => $react['post_id'],
                        'status' => true
                    ])->count();
                    if ($count === 0) {
                        $count = '';
                    } elseif ($count === 1) {
                        $count = $count . ' like';
                    } else {
                        $count = $count . ' likes';
                    }
                    $value = ['status' => $react['status'], 'count' => $count];
                }
            } else {
                $entity['user_id'] = (int)$this->Auth->user('id');
                $entity['status'] = true;
                $react = $this->Likes->patchEntity($react, $entity);
                if ($this->Likes->save($react)) {
                    $count = $this->Likes->find('all')->where([
                        'Likes.post_id' => $react['post_id'],
                        'status' => true
                    ])->count();
                    if ($count === 0) {
                        $count = '';
                    } elseif ($count === 1) {
                        $count = $count . ' like';
                    } else {
                        $count = $count . ' likes';
                    }
                    $value = ['status' => $react['status'], 'count' => $count];
                }
            }

            $likes = $value;
            // Set the view vars that have to be serialized.
            $this->set(compact('likes'));

            // Specify which view vars JsonView should serialize.
            $this->set('_serialize', ['likes']);
        }
    }
}
