<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * ProfilePictures Controller
 *
 *
 * @method \App\Model\Entity\ProfilePicture[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProfilePicturesController extends AppController
{
    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // The add and routes actions are always allowed to logged in users.
        if (in_array($action, ['update'])) {
            return true;
        }

        // All other actions require a id.
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function update()
    {
        $profilePictures = $this->ProfilePictures->newEntity();
        if ($this->request->is('post')) {
            $profilePictures = $this->ProfilePictures->patchEntity($profilePictures, $this->request->getData());
            $profilePictures['user_id'] = $this->Auth->user('id');
            if ($this->ProfilePictures->save($profilePictures)) {
                $user = TableRegistry::getTableLocator()->get('Users');
                $user = $user->get($this->Auth->user('id'));
                $this->Auth->setUser($user);
                $this->Flash->success(__('Well done! Profile picture updated.'));
                return $this->redirect(['controller' => 'users', 'action' => 'view', $this->Auth->user('id')]);
            }
            $this->Flash->error(__('Profile picture could not be saved. Please, try again.'));
        }
        $this->set(compact('profilePictures'));
    }
}
