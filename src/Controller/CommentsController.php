<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 *
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // The add and routes actions are always allowed to logged in users.
        if (in_array($action, ['add', 'commentsAjax'])) {
            return true;
        }

        // All other actions require a id.
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }
        try {
            // Check that the user belongs to the current user.
            $comment = $this->Comments->get($id);

            // boolean result
            return $user['id'] === $comment['user_id'];
        } catch (\Throwable $exception) {
            return false;
        }
    }

    public function commentsAjax($id = null, $recentCommentId = null)
    {
        try {
            $comments = $this->Comments
                ->find('all')
                ->contain([
                    'Users',
                ])
                ->order(['Comments.id' => 'ASC'])
                ->limit(4);
            if ($recentCommentId !== null) {
                $comments->where([
                    'Comments.post_id' => $id,
                    'Comments.status' => true,
                    'Comments.id >' => $recentCommentId
                ]);
            } else {
                $comments->where(['Comments.post_id' => $id, 'Comments.status' => true]);
            }

            // Set the view vars that have to be serialized.
            $hasNext = false;
            $comments = $comments->toArray();
            if (count($comments) > 3) {
                $hasNext = true;
                array_pop($comments);
            }
            foreach ($comments as $comment) {
                $comment['Auth'] = false;
                $comment['hasNext'] = $hasNext;
                if ($comment['user']['id'] === $this->Auth->user('id')) {
                    $comment['Auth'] = true;
                }
            }
        } catch (\Throwable $exception) {
            $comments = 'failed';
        }
        $this->set(compact('comments'));

        // Specify which view vars JsonView should serialize.
        $this->set('_serialize', ['comments']);
    }

    /**
     * @return \Cake\Http\Response
     */
    public function delete($id)
    {
        $comment = $this->Comments->findById($id)->firstOrFail();
        if ($comment->status === true) {
            $comment->status = false;
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('Comment deleted successfully.'));
                return $this->redirect($this->referer());
            }
            foreach ($comment->getErrors() as $errors) {
                foreach ($errors as $error => $value) {
                    $msg = '<strong>Ohh no! Failed to delete comment.</strong> ';
                    $this->Flash->error(__($msg . $value), ['escape' => false]);
                }
            }
        }
        return $this->redirect($this->referer());
    }

    /**
     * @return \Cake\Http\Response
     */
    public function add()
    {
        $comment = $this->Comments->newEntity();
        if ($this->request->is('post')) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());

            $comment->user_id = $this->Auth->user('id');

            if ($this->Comments->save($comment)) {
                $value = ['status' => true];
            } else {
                $value = ['status' => false, 'errors' => $comment->getError('content')];
            }

            $comment = $value;
            // Set the view vars that have to be serialized.
            $this->set(compact('comment'));

            // Specify which view vars JsonView should serialize.
            $this->set('_serialize', ['comment']);
        }
    }
}
