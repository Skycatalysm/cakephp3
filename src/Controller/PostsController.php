<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{

    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        // The add and routes actions are always allowed to logged in users.
        if (in_array($action, ['userPost', 'edit', 'share'])) {
            return true;
        }

        // All other actions require a id.
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }
        try {
            // Check that the user belongs to the current user.
            $post = $this->Posts->get($id);

            // boolean result
            return $user['id'] === $post['user_id'];
        } catch (\Throwable $exception) {
            return false;
        }
    }

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow('feed');
        parent::beforeFilter($event);
    }

    /**
     * @param null $id
     * @return \Cake\Http\Response|null
     */
    public function edit()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newEntity = $this->request->getData();
            $post = $this->Posts->findById($newEntity['Post']['id'])->firstOrFail();
            if (!((int)$post['user_id'] === (int)$this->Auth->user('id'))) {
                $this->Flash->error(__('You are not authorized to access that location.'));
                return $this->redirect($this->referer());
            }
            $post = $this->Posts->patchEntity($post, $newEntity['Post']);
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Post updated successfully.'));
                return $this->redirect($this->referer());
            }
            foreach ($post->getErrors() as $errors) {
                foreach ($errors as $error => $value) {
                    $this->Flash->error(__('<strong>Ohh no! Failed to post.</strong> ' . $value), ['escape' => false]);
                }
            }
        }
        return $this->redirect($this->referer());
    }

    /**
     * @param null $id
     * @return \Cake\Http\Response|null
     */
    public function delete($id)
    {
        if ($this->request->is(['get'])) {
            $post = $this->Posts->get($id);
            if ($post->status === true) {
                $post->set('status', false);
                if ($this->Posts->save($post)) {
                    $this->Flash->success(__('Post deleted successfully.'), ['escape' => false]);
                    return $this->redirect($this->referer());
                }
                $this->Flash->error(__('Something went wrong. Post can\'t be deleted.'), ['escape' => false]);
            }
        }
        return $this->redirect($this->referer());
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function share()
    {
        if ($this->request->is(['post'])) {
            $entity = $this->request->getData();
            $post = $this->Posts->newEntity();
            try {
                $existChecker = $this->Posts->get($entity['shared_post_id']);
                if ($existChecker->get('status') === false) {
                    $msg = '<strong>Something went wrong.</strong> Post doesn\'t exist.';
                    $this->Flash->error(__($msg), ['escape' => false]);
                    return $this->redirect($this->referer());
                }
            } catch (\Throwable $exception) {
                $msg = '<strong>Something went wrong.</strong> Post doesn\'t exist.';
                $this->Flash->error(__($msg), ['escape' => false]);
                return $this->redirect($this->referer());
            }
            $post = $this->Posts->patchEntity($post, $entity);
            $post->set('user_id', $this->Auth->user('id'));
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('<strong>Well done!</strong> Shared the post.'), ['escape' => false]);
                return $this->redirect($this->referer());
            }
            foreach ($post->getErrors() as $errors) {
                foreach ($errors as $error => $value) {
                    $this->Flash->error(__('<strong>Ohh no! Failed to post.</strong> ' . $value), ['escape' => false]);
                }
            }
        }
        return $this->redirect($this->referer());
    }

    /**
     * @param null $id
     * @param null $recentPostId
     */
    public function userPost($id = null, $recentPostId = null)
    {
        try {
            $posts = $this->Posts->findByUserId($id)
                ->contain([
                    'Users',
                    'Likes',
                    'Comments' => function ($q) {
                        return $q
                            ->order(['Comments.created' => 'DESC'])
                            ->where(['Comments.status' => true])
                            ->group(['Comments.post_id']);
                    },
                    'SharedPost' => function ($q) {
                        return $q->where(['SharedPost.status' => true])->contain('Users');
                    }
                ])
                ->order(['Posts.id' => 'DESC'])
                ->limit(4);
            if ($recentPostId !== null) {
                $posts->where(['Posts.status' => true, 'Posts.id <' => $recentPostId]);
            } else {
                $posts->where(['Posts.status' => true]);
            }
            $posts = $posts->toArray();
            $hasNext = false;
            if (count($posts) > 3) {
                array_pop($posts);
                $hasNext = true;
            }
            foreach ($posts as $post) {
                $post['hasNext'] = $hasNext;
                $post['Auth'] = false;
                if ($post['user_id'] === $this->Auth->user('id')) {
                    $post['Auth'] = true;
                }
                $post['shared_auth'] = false;
                if (isset($post['shared_post']['user']['id']) &&
                    (int)$post['shared_post']['user']['id'] === $this->Auth->user('id')) {
                    $post['shared_auth'] = true;
                }
                $post['React'] = false;
                $post['ReactCount'] = '';
                $count = 0;
                foreach ($post['likes'] as $like) {
                    if ($like['user_id'] === $this->Auth->user('id') && $like['status'] === true) {
                        $post['React'] = true;
                    }
                    if ($like['status'] === true) {
                        $count++;
                    }
                }
                if ($count === 0) {
                    $post['ReactCount'] = '';
                } elseif ($count === 1) {
                    $post['ReactCount'] = $count . ' like';
                } else {
                    $post['ReactCount'] = $count . ' likes';
                }
            }
            // Set the view vars that have to be serialized.
            $this->set(compact(['posts']));

            // Specify which view vars JsonView should serialize.
            $this->set('_serialize', ['posts']);
        } catch (\Throwable $exception) {
            $posts = null;
            // Set the view vars that have to be serialized.
            $this->set(compact('posts'));

            // Specify which view vars JsonView should serialize.
            $this->set('_serialize', ['posts']);
        }
    }

    /**
     * News feed view
     */
    public function feed()
    {
        $postEntity = $this->Posts->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newEntity = $this->request->getData();
            $postEntity = $this->Posts->patchEntity($postEntity, $newEntity);
            $postEntity->set('user_id', $this->Auth->user('id'));
            if ($this->Posts->save($postEntity)) {
                $this->Flash->success(__('Posted successfully!'));
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Post failed. Please, try again.'));
        }
        //Check if logged in
        $id = $this->Auth->user('id');
        if (!isset($id)) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        //Get all followers
        $followedUser = TableRegistry::getTableLocator()->get('Followers');
        $followedUser = $followedUser->find('all')
            ->where(['Followers.user_id' => (int)$this->Auth->user('id'), 'Followers.status' => true]);


        $conditions = array();
        foreach ($followedUser as $value) {
            $conditions[] = array('Posts.user_id' => $value['followed_user_id']);
        }
        $conditions[] = array('Posts.user_id' => $this->Auth->User('id'));

        $posts = $this->paginate($this->Posts
            ->find('all')
            ->contain([
                'Users',
                'Comments' => function ($q) {
                    return $q
                        ->order(['Comments.created' => 'DESC'])
                        ->where(['Comments.status' => true])
                        ->group(['Comments.post_id']);
                },
                'Likes' => function ($q) {
                    return $q->where(['Likes.status' => true]);
                },
                'SharedPost' => function ($q) {
                    return $q->where(['SharedPost.status' => true])->contain('Users');
                }
            ])
            ->where(['Posts.status' => true])
            ->order(['Posts.id' => 'DESC']), array(
            'limit' => 3,
            'conditions' => array(
                'OR' => $conditions
            ),
        ));

        foreach ($posts as $post) {
            $likedCheck = array_filter(
                $post['likes'],
                function ($item) {
                    return $item['user_id'] === $this->Auth->user('id');
                }
            );
            $post['liked'] = (!empty($likedCheck));
        }
        $this->set(compact(['posts', 'postEntity']));
    }
}
