<?php

namespace App\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Validation\Validator;
use Josegonzalez\Upload\Validation\DefaultValidation;

/**
 * Posts Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\SharedPostsTable&\Cake\ORM\Association\BelongsTo $SharedPosts
 *
 * @method \App\Model\Entity\Post get($primaryKey, $options = [])
 * @method \App\Model\Entity\Post newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Post[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Post|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Post[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Post findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PostsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('posts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasOne('SharedPost', [
            'className' => 'Posts'
        ])->setForeignKey('shared_post_id');
        $this->hasMany('Likes');
        $this->hasMany('Comments');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SharedPost', [
            'className' => 'Posts'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'image_name' => [
                'fields' => [
                    'dir' => 'photo_dir',
                    'size' => 'photo_size',
                    'type' => 'photo_type'
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::uuid() . '.' . $extension;
                    return $data['name'];
                },
            ]
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->setProvider('upload', DefaultValidation::class);

        $validator
            ->allowEmptyFile('image_name')
            ->add('image_name', 'fileBelowMaxSize', [
                'rule' => ['isBelowMaxSize', 2000000],
                'message' => 'This file is too large',
                'provider' => 'upload'
            ])
            ->add('image_name', 'fileCompletedUpload', [
                'rule' => 'isCompletedUpload',
                'message' => 'This file could not be uploaded completely',
                'provider' => 'upload'
            ])
            ->add('image_name', 'fileFileUpload', [
                'rule' => 'isFileUpload',
                'message' => 'There was no file found to upload',
                'provider' => 'upload'
            ]);

        $validator->add('image_name', 'custom', [
            'rule' => function ($value, $context) {
                $allowedFiles = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                $extension = pathinfo($context['data']['image_name']['name'], PATHINFO_EXTENSION);
                return in_array($extension, $allowedFiles);
            },
            'message' => 'Only (png, jpg, jpeg) files allowed'
        ]);

        $validator->add('image_name', 'UPLOAD_ERR_INI_SIZE', [
            'rule' => function ($value, $context) {
                if ((int)$context['data']['image_name']['error'] === 1) {
                    return false;
                };
                return true;
            },
            'message' => 'This file exceeds the upload max file size'
        ]);

        $validator->add('image_name', 'UPLOAD_ERR_FORM_SIZE', [
            'rule' => function ($value, $context) {
                if ((int)$context['data']['image_name']['error'] === 2) {
                    return false;
                };
                return true;
            },
            'message' => 'This file exceeds the upload max file size'
        ]);

        $validator->add('image_name', 'UPLOAD_ERR_PARTIAL', [
            'rule' => function ($value, $context) {
                if ((int)$context['data']['image_name']['error'] === 3) {
                    return false;
                };
                return true;
            },
            'message' => 'The uploaded file was only partially uploaded'
        ]);

        $validator->add('image_name', 'UPLOAD_ERR_NO_FILE', [
            'rule' => function ($value, $context) {
                if ((int)$context['data']['image_name']['error'] === 4) {
                    return false;
                };
                return true;
            },
            'message' => 'No file was uploaded'
        ]);

        $validator->add('image_name', 'UPLOAD_ERR_NO_TMP_DIR', [
            'rule' => function ($value, $context) {
                if ((int)$context['data']['image_name']['error'] === 6) {
                    return false;
                };
                return true;
            },
            'message' => 'Missing a temporary folder'
        ]);

        $validator->add('image_name', 'UPLOAD_ERR_CANT_WRITE', [
            'rule' => function ($value, $context) {
                if ((int)$context['data']['image_name']['error'] === 7) {
                    return false;
                };
                return true;
            },
            'message' => 'Failed to write the uploaded file'
        ]);

        $validator->add('image_name', 'UPLOAD_ERR_EXTENSION', [
            'rule' => function ($value, $context) {
                if ((int)$context['data']['image_name']['error'] === 8) {
                    return false;
                };
                return true;
            },
            'message' => 'Something went wrong, extension stopped the file upload'
        ]);


        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('content')
            ->maxLength('content', 140, 'Exceeded max character limit of 140')
            ->requirePresence('content', 'create')
            ->notEmptyString('content')
            ->add('content', 'custom', [
                'rule' => function ($value, $context) {
                    if (strlen(trim($context['data']['content'])) === 0) {
                        return false;
                    }
                    return true;
                },
                'message' => 'This field cannot be left empty'
            ]);


        $validator
            ->boolean('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
