<?php

namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->hasOne('ProfilePictures');
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 50, 'Characters must not be more than 50')
            ->requirePresence('username', 'create')
            ->notEmptyString('username')
            ->add('username', 'custom', [
                'rule' => function ($value, $context) {
                    if (strlen(trim($context['data']['username'])) === 0) {
                        return false;
                    }
                    return true;
                },
                'message' => 'This field cannot be left empty'
            ]);

        $validator
            ->email('email', null, 'Not a valid email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 255, 'Characters must not be more than 255')
            ->requirePresence('password', 'create')
            ->notEmptyString('password')
            ->minLength('password', 8, 'Must be at least 8 characters');

        $validator
            ->add('confirm_password', 'no-misspelling', [
                'rule' => ['compareWith', 'password'],
                'message' => 'Passwords are not equal',
            ]);
        $validator
            ->add('current_password', 'no-misspelling', [
                'rule' => function ($value, $context) {
                    $id = $context['data']['id'];
                    $password = $this->get($id)->get('password');
                    $oldPassword = $context['data']['current_password'];
                    $result = (new DefaultPasswordHasher)->check($oldPassword, $password);

                    return $result;
                },
                'message' => 'Password is incorrect.',
            ])
            ->notEmptyString('current_password');
        $validator
            ->add('first_name', 'emptyFirstName', [
                'rule' => function ($value, $context) {
                    if (strlen(trim($context['data']['first_name'])) === 0) {
                        return false;
                    }
                    return true;
                },
                'message' => 'This field cannot be left empty'
            ])
            ->add('first_name', [
                'alphaNumericWithSpaces' => [
                    'rule' => ['custom', '/^[a-z ]*$/i'],
                    'message' => 'Please supply a valid first name'
                ]
            ])
            ->add('first_name', 'custom', [
                'rule' => function ($value) {
                    return (bool)!preg_match("/\w+\s{2,}(?=\w+)/", $value);
                },
                'message' => 'Please supply a valid first name'
            ])
            ->scalar('first_name')
            ->maxLength('first_name', 50, 'Characters must not be more than 50')
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->add('last_name', 'custom', [
                'rule' => function ($value) {
                    return ctype_alpha($value);
                },
                'message' => 'Please supply a valid last name'
            ])
            ->scalar('last_name')
            ->maxLength('last_name', 50, 'Characters must not be more than 50')
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name');

        $validator
            ->boolean('status')
            ->notEmptyString('status');

        $validator
            ->scalar('email_hash')
            ->maxLength('email_hash', 255, 'Characters must not be more than 255')
            ->requirePresence('email_hash', 'create')
            ->notEmptyString('email_hash');

        $validator->add('username', 'unique', [
            'rule' => 'validateUnique',
            'provider' => 'table'
        ]);

        $validator->add('email', 'unique', [
            'rule' => 'validateUnique',
            'provider' => 'table'
        ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
