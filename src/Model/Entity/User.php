<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $updated
 * @property bool $status
 * @property string $email_hash
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'email' => true,
        'password' => true,
        'first_name' => true,
        'last_name' => true,
        'created' => true,
        'updated' => true,
        'status' => true,
        'email_hash' => true
    ];
    protected $_virtual = ['full_name', 'truncated_full_name', 'profile_picture'];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    /**
     * @param $firstName
     * @return string
     */
    protected function _setFirstName($firstName)
    {
        $firstName = trim($firstName);
        return ucwords($firstName);
    }

    /**
     * @param $lastName
     * @return string
     */
    protected function _setLastName($lastName)
    {
        return ucwords($lastName);
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($value);
        }
    }

    /**
     * @return string
     */
    protected function _getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return string
     */
    protected function _getTruncatedFullName()
    {
        return Text::truncate($this->first_name, 20, [
                'ellipsis' => '...',
                'exact' => false
            ]) . ' ' . Text::truncate($this->last_name, 20, [
                'ellipsis' => '...',
                'exact' => false
            ]);
    }

    public function _getProfilePicture()
    {
        $profilePicture = TableRegistry::getTableLocator()->get('ProfilePictures');
        $profilePicture = $profilePicture->findByUserId($this->id)->last();
        try {
            $profileUrl = '/files/ProfilePictures/image_name/' . $profilePicture->get('image_name');
        } catch (\Throwable $exception) {
            $profileUrl = '/files/ProfilePictures/image_name/default_profile_picture.png';
        }
        return $profileUrl;
    }

}
