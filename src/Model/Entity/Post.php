<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Post Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $shared_post_id
 * @property string $content
 * @property string|null $image_name
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property bool $status
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\SharedPost $shared_post
 */
class Post extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'shared_post_id' => true,
        'content' => true,
        'image_name' => true,
        'created' => true,
        'modified' => true,
        'status' => true,
        'user' => true,
        'shared_post' => true
    ];
    protected $_virtual = ['image_url'];

    /**
     * @return string
     */
    protected function _getImageUrl()
    {
        if ($this->image_name !== null) {
            return '/files/Posts/image_name/' . $this->image_name;
        }
        return null;
    }

}
