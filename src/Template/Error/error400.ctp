<?php
$this->assign('title', h('Missing Page'));
?>
<div class="row bg-light p-5 rounded border shadow-sm">
    <div class="col-12 text-center">
        <h3 class="text-primary mb-3">
            We can't seem to find this page
        </h3>
        <p class="text-info">
            Looks like the page you're looking for doesn't exist.
            <br/>
            Return to the homepage by clicking the button below.
        </p>
        <?php
        echo $this->Html->link(__('Back to homepage'),
            array('controller' => 'posts', 'action' => 'feed'),
            array('class' => 'btn btn-primary btn-lg')
        )
        ?>
    </div>
</div>
