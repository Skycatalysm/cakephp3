<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?= $this->Form->create(null, array('class' => 'form-checker')) ?>
<div class="row justify-content-md-center">
    <div class="col-12">
        <?= $this->element('Forms/page_titles', array('title' => __('Change Password'))) ?>
    </div>
    <div class="col-md-6">
        <?php
        $formName = 'password';
        $classList = 'form-control form-required';
        $errorMessage = array();
        if ($user->getError($formName)) {
            foreach ($user->getError($formName) as $item => $value) {
                $errorMessage[] = '<small class="text-danger">' . $value . '</small>';
            }
            $classList = $classList . ' is-invalid';
        }
        $placeHolder = 'Enter new password';
        echo $this->element('Forms/form_group', array(
            'formName' => $formName,
            'classList' => $classList,
            'placeHolder' => $placeHolder,
            'errorMessage' => $errorMessage
        ));
        ?>
        <?php
        $formName = 'confirm_password';
        $classList = 'form-control form-required';
        $errorMessage = array();
        $type = 'password';
        if ($user->getError($formName)) {
            foreach ($user->getError($formName) as $item => $value) {
                $errorMessage[] = '<small class="text-danger">' . $value . '</small>';
            }
            $classList = $classList . ' is-invalid';
        }
        $placeHolder = 'Re-enter new password';
        echo $this->element('Forms/form_group', array(
            'formName' => $formName,
            'classList' => $classList,
            'placeHolder' => $placeHolder,
            'errorMessage' => $errorMessage,
            'type' => $type
        ));
        ?>
        <?php
        $formName = 'current_password';
        $classList = 'form-control form-required';
        $errorMessage = array();
        $type = 'password';
        if ($user->getError($formName)) {
            foreach ($user->getError($formName) as $item => $value) {
                $errorMessage[] = '<small class="text-danger">' . $value . '</small>';
            }
            $classList = $classList . ' is-invalid';
        }
        $placeHolder = 'Enter current password';
        echo $this->element('Forms/form_group', array(
            'formName' => $formName,
            'classList' => $classList,
            'placeHolder' => $placeHolder,
            'errorMessage' => $errorMessage,
            'type' => $type
        ));
        ?>
    </div>
    <div class="col-12 text-center">
        <?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary px-5')) ?>
    </div>
</div>
<?= $this->Form->end() ?>
