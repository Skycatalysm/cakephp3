<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $users
 */
?>
<div class="d-flex flex-row justify-content-center mb-3">
    <?= $this->element('Forms/page_titles', array('title' => __('Find People'))) ?>
    <span class="text-dark ml-1">(<?php
        echo $this->Paginator->params()['count'];
        echo ($this->Paginator->params()['count'] > 1) ? ' results' : ' result';
        ?>)</span>
</div>
<?php if ($this->Paginator->params()['count'] > 0): ?>
    <div class="row justify-content-md-center">
        <?php foreach ($users as $user): ?>
            <?php
            $follower['user'] = $user;
            $follower['followed'] = $user['followed'];
            ?>
            <?= $this->element('Followers/follow_user', compact('follower')) ?>
        <?php endforeach; ?>
    </div>
    <?= $this->element('Followers/follow') ?>
<?php else: ?>
    <div class="p-2 my-3 rounded border border-light shadow-sm">
        <div class="row">
            <div class="col-md-12 p-5 text-center">
                <h3 class="font-weight-bold text-info">Did not find anyone</h3>
                <p class="text-secondary">We can't seem to find the person you are looking for.</p>
            </div>
        </div>
    </div>
<?php endif; ?>
<?= $this->element('Pagination/pagination') ?>
