<?= $this->Form->create(null, array('class' => 'form-checker')) ?>
<div class="row justify-content-md-center">
    <div class="col-md-6">
        <?= $this->element('Forms/page_titles', array('title' => __('Login'))) ?>
        <?php
        $formName = 'username';
        $classList = 'form-control form-required';
        $errorMessage = array();
        $placeHolder = 'Enter username';
        echo $this->element('Forms/form_group', array(
            'formName' => $formName,
            'classList' => $classList,
            'placeHolder' => $placeHolder,
            'errorMessage' => $errorMessage
        ));
        ?>
        <?php
        $formName = 'password';
        $classList = 'form-control form-required';
        $errorMessage = array();
        $placeHolder = 'Enter password';
        echo $this->element('Forms/form_group', array(
            'formName' => $formName,
            'classList' => $classList,
            'placeHolder' => $placeHolder,
            'errorMessage' => $errorMessage
        ));
        ?>
        <div class="text-center">
            <?= $this->Form->button(__('Login'), array('class' => 'btn btn-primary px-5')) ?>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
