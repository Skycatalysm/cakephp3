<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?= $this->Form->create($user, array('class' => 'form-checker')) ?>
<div class="row justify-content-md-center">
    <div class="col-12">
        <?= $this->element('Forms/page_titles', array('title' => __('Update Information'))) ?>
    </div>
    <div class="col-md-6">
        <?php
        $formName = 'username';
        $classList = 'form-control form-required';
        $errorMessage = array();
        if ($user->getError($formName)) {
            foreach ($user->getError($formName) as $item => $value) {
                $errorMessage[] = '<small class="text-danger">' . $value . '</small>';
            }
            $classList = $classList . ' border border-danger';
        }
        $placeHolder = 'Enter username';
        echo $this->element('Forms/form_group', array(
            'formName' => $formName,
            'classList' => $classList,
            'placeHolder' => $placeHolder,
            'errorMessage' => $errorMessage
        ));
        ?>
    </div>
    <div class="col-md-6">
        <?php
        $formName = 'email';
        $classList = 'form-control form-required';
        $errorMessage = array();
        if ($user->getError($formName)) {
            foreach ($user->getError($formName) as $item => $value) {
                $errorMessage[] = '<small class="text-danger">' . $value . '</small>';
            }
            $classList = $classList . ' border border-danger';
        }
        $placeHolder = 'Enter email';
        echo $this->element('Forms/form_group', array(
            'formName' => $formName,
            'classList' => $classList,
            'placeHolder' => $placeHolder,
            'errorMessage' => $errorMessage
        ));
        ?>
    </div>
    <div class="col-md-6">
        <?php
        $formName = 'first_name';
        $classList = 'form-control form-required';
        $errorMessage = array();
        if ($user->getError($formName)) {
            foreach ($user->getError($formName) as $item => $value) {
                $errorMessage[] = '<small class="text-danger">' . $value . '</small>';
            }
            $classList = $classList . ' border border-danger';
        }
        $placeHolder = 'Enter first name';
        echo $this->element('Forms/form_group', array(
            'formName' => $formName,
            'classList' => $classList,
            'placeHolder' => $placeHolder,
            'errorMessage' => $errorMessage
        ));
        ?>
    </div>
    <div class="col-md-6">
        <?php
        $formName = 'last_name';
        $classList = 'form-control form-required';
        $errorMessage = array();
        if ($user->getError($formName)) {
            foreach ($user->getError($formName) as $item => $value) {
                $errorMessage[] = '<small class="text-danger">' . $value . '</small>';
            }
            $classList = $classList . ' border border-danger';
        }
        $placeHolder = 'Enter last name';
        echo $this->element('Forms/form_group', array(
            'formName' => $formName,
            'classList' => $classList,
            'placeHolder' => $placeHolder,
            'errorMessage' => $errorMessage
        ));
        ?>
    </div>
    <div class="col-12 text-center">
        <?= $this->Form->button(__('Submit'), array('class' => 'btn btn-primary px-5')) ?>
    </div>
</div>
<?= $this->Form->end() ?>
