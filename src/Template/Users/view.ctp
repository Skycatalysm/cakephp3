<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 * @var \App\Model\Entity\Follower $follow
 */
?>
<?php $this->assign('title', h($user['full_name'])); ?>
<?= $this->element('User/lg_user_info', compact($user, $follow)) ?>
<?php
if ($this->Session->read('Auth.User.id') === (int)$user['id']) {
    echo $this->element('Post/post_form');
}
?>
<?= $this->element('Followers/follow') ?>
<?= $this->element('Comments/comments_list') ?>
<?= $this->element('Comments/comment') ?>
<?= $this->element('Comments/comment_form') ?>
<?php $this->append('js_block', $this->Html->script('user_view.js')); ?>
<?php $this->append('js_block', $this->Html->script('modals.js')); ?>
<?= $this->element('Post/posts_block') ?>
<?= $this->element('Modal/post_edit') ?>
<?= $this->element('Modal/post_delete') ?>
<?= $this->element('Modal/post_share') ?>
<?= $this->element('Modal/comment_delete') ?>
<?= $this->element('Likes/react') ?>

