<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Like $likes
 */
?>

<?php
echo json_encode(compact('likes'));
?>
