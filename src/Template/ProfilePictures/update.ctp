<?php
/**
 * @var $profilePictures \App\Model\Entity\ProfilePicture
 */
?>
<?= $this->Form->create($profilePictures, array('class' => 'form-checker', 'type' => 'file')) ?>
<div class="row justify-content-center">
    <div class="col-6">
        <?= $this->element('Forms/page_titles', array('title' => __('Update Profile Picture'))) ?>
        <div class="form-group text-center d-flex justify-content-center">
            <div class="profile-picture-container border border-light">
                <?= $this->Html->image($this->Session->read('Auth.User.profile_picture'), array('alt' => 'Profile-picture', 'class' => 'img-fluid comment-user-picture')); ?>
            </div>
        </div>
        <?php
        $formName = 'image_name';
        $classList = 'form-control mt-0 form-required dropify';
        $errorMessage = array();
        $type = 'file';
        $label = 'Image';
        if ($profilePictures->getError($formName)) {
            foreach ($profilePictures->getError($formName) as $item => $value) {
                $errorMessage[] = '<small class="text-danger">' . $value . '</small>';
            }
            $classList = $classList . ' is-invalid';
        }
        $placeHolder = null;
        ?>
        <div class="form-group">
            <?php
            echo $this->Form->control($formName, array(
                'class' => $classList,
                'placeholder' => $placeHolder,
                'required' => false,
                'error' => false,
                'type' => $type,
                'label' => $label,
                'data-max-file-size' => '2M',
                'data-allowed-file-extensions' => 'png jpg jpeg PNG JPG JPEG'
            ));
            ?>
            <div class="form-messages">
                <?php
                foreach ($errorMessage as $msg) {
                    echo $msg;
                }
                ?>
            </div>
        </div>
        <div class="text-center">
            <?= $this->Form->button(__('Upload'), array('class' => 'btn btn-primary px-5')) ?>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
