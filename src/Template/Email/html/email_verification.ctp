<?php

use Cake\Routing\Router;

?>

<?php $root = Router::fullbaseUrl() ?>

<div style="border: solid #2c3e50;padding: 5rem;">
    <h3>Welcome to microblog!</h3>
    <p style="color:#18bc9c"><b>
            Yay! Welcome to microblog! Now let's confirm your account so we can start your all-access pass.
        </b></p>
    <a href="<?php echo $root ?>/users/verify/<?php echo $content; ?>">
        <button
            style="border-radius: 5px;background-color: #2c3e50;border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer;">
            Click here!
        </button>
    </a>
</div>
