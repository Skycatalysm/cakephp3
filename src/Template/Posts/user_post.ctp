<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $posts
 */
?>

<?php
echo json_encode(compact('posts'));
?>
