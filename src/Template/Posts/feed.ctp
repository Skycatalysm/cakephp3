<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $posts
 */
?>
<div class="d-flex flex-row justify-content-center mb-3">
    <?= $this->element('Forms/page_titles', array('title' => __('News Feed'))) ?>
    <span class="text-dark ml-1">
        (<?php
        echo $this->Paginator->params()['count'];
        echo ($this->Paginator->params()['count'] > 1) ? ' results' : ' result';
        ?>)
	</span>
</div>
<?= $this->element('Pagination/pagination') ?>

<?php
if ($this->Session->read('Auth.User.id')) :?>
    <div class="my-3">
        <?= $this->element('Post/post_form'); ?>
    </div>
<?php endif; ?>
<?php if ($this->Paginator->params()['count'] > 0): ?>
    <?php foreach ($posts as $post): ?>
        <?= $this->element('Post/feed_post', compact('post')) ?>
    <?php endforeach; ?>

    <?php $this->append('js_block', $this->Html->script('modals.js')); ?>
    <?php $this->append('js_block', $this->Html->script('post_feed.js')); ?>
    <?= $this->element('Comments/comments_list') ?>
    <?= $this->element('Comments/comment') ?>
    <?= $this->element('Comments/comment_form') ?>
    <?= $this->element('Modal/post_edit') ?>
    <?= $this->element('Modal/post_delete') ?>
    <?= $this->element('Modal/post_share') ?>
    <?= $this->element('Modal/comment_delete') ?>
    <?= $this->element('Likes/react') ?>
<?php else: ?>
    <div class="p-2 my-3 rounded border border-light shadow-sm">
        <div class="row">
            <div class="col-md-12 p-5 text-center">
                <h3 class="font-weight-bold text-info">No posts yet</h3>
                <p class="text-secondary">Post something or follow someone.</p>
            </div>
        </div>
    </div>
<?php endif; ?>
<?= $this->element('Pagination/pagination') ?>
