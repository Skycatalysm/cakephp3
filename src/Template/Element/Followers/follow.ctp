<?php
/**
 * @var \App\Model\Entity\User $user
 */

if (!isset($user['id'])) {
    $user['id'] = '';
}
?>
<div class="d-none">
    <?php echo $this->Form->create('Followers', array(
        'id' => 'follow-form',
        'url' => array(
            'controller' => 'followers',
            'action' => 'follow'
        ),
    ));
    ?>
    <?php
    $this->Form->unlockField('followed_user_id');
    echo $this->Form->control('followed_user_id', array(
        'value' => $user['id'],
        'type' => 'hidden',
        'id' => 'followed_user_id'
    )); ?>
    <?php echo $this->Form->button(__('save'), array('class' => 'btn btn-success d-none')); ?>
    <?php echo $this->Form->end(); ?>
</div>
<?php
$this->append('js_block', $this->Html->script('follow.js'));
?>
