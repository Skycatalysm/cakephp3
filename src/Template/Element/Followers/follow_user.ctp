<?php
/**
 * @var \App\Model\Entity\Follower $follower
 */
?>
<div class="col-xl-4 col-lg-6 col-sm-12 mb-3">
    <div class="border rounded border-dark shadow-sm p-2 h-100">
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="p-2">
                <div class="profile-picture-post-container border border-light">
                    <?= $this->Html->image(
                        $follower['user']['profile_picture'],
                        array(
                            'alt' => 'Profile-picture',
                            'class' => 'img-fluid comment-user-picture',
                            'id' => 'post-user-profile-picture'
                        )); ?>
                </div>
            </div>
            <div class="mr-auto p-2">
                <?php
                echo $this->Html->link(h($follower['user']['full_name']),
                    array('controller' => 'users', 'action' => 'view', $follower['user']['id']),
                    array('class' => 'font-weight-bold text-primary flexible-text')
                )
                ?>
                <br>
                <?php if ($follower['user']['id'] !== (int)$this->Session->read('Auth.User.id')): ?>
                    <a href="mailto:<?= h($follower['user']['email']) ?>">

                        <small class="text-success flexible-text">
                            <?= h($follower['user']['email']) ?>
                        </small>
                    </a>
                <?php else:; ?>
                    <a href="#" class="email-trivia" onclick="return false">
                        <small class="text-success flexible-text">
                            <?= h($follower['user']['email']) ?>
                        </small>
                    </a>
                <?php endif; ?>
            </div>
            <div>
                <?php if ($follower['user']['id'] !== (int)$this->Session->read('Auth.User.id')): ?>
                    <?php if ($follower['followed'] === true): ?>
                        <button data-followed-user-id="<?= $follower['user']['id'] ?>"
                                class="btn btn-sm btn-primary px-3 ajax-follow">
                            unfollow
                        </button>
                    <?php elseif ($follower['followed'] === false): ?>
                        <button data-followed-user-id="<?= $follower['user']['id'] ?>"
                                class="btn btn-sm btn-primary px-3 ajax-follow">
                            follow
                        </button>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
