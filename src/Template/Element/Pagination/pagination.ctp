<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="p-2 rounded border border-light shadow-sm">
    <div class="row">
        <div class="col-md-8 d-flex justify-content-center justify-content-md-start">
            <div class="btn-group btn-group-sm" role="group">
                <?php
                $this->Paginator->setTemplates([
                    'prevActive' => '<a class="btn btn-outline-success" href="{{url}}"><small>{{text}}</small></a>',
                    'prevDisabled' => '<a class="btn btn-outline-success" href="{{url}}"><small>{{text}}</small></a>',
                    'number' => '<a class="btn btn-outline-success" href="{{url}}"><small>{{text}}</small></a>',
                    'current' => '<a class="btn btn-success" href="{{url}}"><small>{{text}}</small></a>',
                    'nextActive' => '<a class="btn btn-outline-success" href="{{url}}"><small>{{text}}</small></a>',
                    'nextDisabled' => '<a class="btn btn-outline-success" href="{{url}}"><small>{{text}}</small></a>',
                    'first' => '<a class="btn btn-outline-success" href="{{url}}"><small>{{text}}</small></a>',
                    'last' => '<a class="btn btn-outline-success" href="{{url}}"><small>{{text}}</small></a>',
                ]); ?>
                <?= $this->Paginator->first() ?><?= $this->Paginator->numbers() ?><?= $this->Paginator->last() ?>
            </div>
        </div>
        <div
            class="<?= ($this->Paginator->params()['pageCount'] > 1) ? 'col-md-4 text-md-right' : 'col-12' ?> text-center">
            <small
                class="text-primary m-0 font-weight-bold"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}')]) ?></small>
        </div>
    </div>
</div>
