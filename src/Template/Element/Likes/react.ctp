<div class="d-none">
    <?php echo $this->Form->create('Like', array(
        'id' => 'like-form',
        'url' => array(
            'controller' => 'likes',
            'action' => 'react'
        ),
    ));
    ?>
    <?php
    $this->Form->unlockField('post_id');
    echo $this->Form->control('post_id', array(
        'type' => 'hidden',
        'id' => 'data-like-id-target'
    )); ?>
    <?php echo $this->Form->button(__('save'), array('class' => 'btn btn-success d-none')); ?>
    <?php echo $this->Form->end(); ?>
</div>
<?php
$this->append('js_block', $this->Html->script('like.js'));
?>
