<div class="modal fade" id="sharePostModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php echo $this->Form->create('Post', array(
                'class' => 'form-checker',
                'url' => array(
                    'controller' => 'posts',
                    'action' => 'share'
                )
            ));
            ?>
            <div class="modal-header bg-success">
                <h5 class="modal-title text-white" id="exampleModalLabel">Share Post</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->control('content', array(
                    'maxLength' => 140,
                    'rows' => 2,
                    'class' => 'form-control mb-2 form-required',
                    'label' => false,
                    'placeholder' => 'Say something...'
                )); ?>
                <?php
                $this->Form->unlockField('shared_post_id');
                echo $this->Form->control('shared_post_id', array(
                    'type' => 'hidden',
                    'id' => 'shareIdTarget'
                )); ?>
                <div class="form-messages">
                </div>
                <div id="share-post-content-target"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <?php echo $this->Form->button(__('share'), array(
                    'class' => 'btn btn-success'
                )); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
