<!-- Modal -->
<div class="modal fade " id="trivia-modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border border-0">
            <div class="modal-header bg-info text-white">
                <h5 class="modal-title">Info</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <strong>Did you know?</strong><br/>
                <p id="trivia-modal-content" class="px-5 blockquote-footer text-primary"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
