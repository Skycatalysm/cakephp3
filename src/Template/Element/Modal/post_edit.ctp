<div class="modal fade" id="editPostModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php echo $this->Form->create(null, array(
                'class' => 'form-checker',
                'url' => array(
                    'controller' => 'posts',
                    'action' => 'edit'
                ),
            ));
            ?>
            <div class="modal-header bg-success">
                <h5 class="modal-title text-white" id="exampleModalLabel">Edit Post</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->control('Post.content', array(
                    'rows' => 3,
                    'maxLength' => 140,
                    'class' => 'form-control mb-2 form-required',
                    'label' => false,
                    'placeholder' => 'Say something...',
                    'id' => 'data-post-content-target',
                )); ?>
                <?php
                $this->Form->unlockField('Post.id');
                echo $this->Form->control('Post.id', array(
                    'type' => 'hidden',
                    'id' => 'data-post-id-target'
                )); ?>
                <div class="form-messages">
                </div>
                <div id="data-post-shared-content-target"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">
                    Close
                </button>
                <?php echo $this->Form->button(__('save'), array('class' => 'btn btn-success')); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
