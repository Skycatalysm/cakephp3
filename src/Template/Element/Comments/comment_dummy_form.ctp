<div class="border p-3 mb-5 bg-white rounded">
    <div class="d-flex flex-row">
        <div class="m-1">
            <div class="profile-picture-comment-container">
                <?= $this->Html->image($this->Session->read('Auth.User.profile_picture'),
                    array(
                        'alt' => 'Profile-picture',
                        'class' => 'img-fluid'
                    )); ?>
            </div>
        </div>
        <div class="w-100 m-1">
            <textarea id="comment-input" rows="2" class="form-control mb-2"
                      placeholder="Say something..."
                      maxlength="140"></textarea>
            <div class="mb-2">
                <button id="post-comment-button" class="btn btn-outline-success btn-sm ajax-comment float-right"
                        type="button">
                    submit
                </button>
            </div>
        </div>
    </div>
</div>

