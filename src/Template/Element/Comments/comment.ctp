<div class="d-none">
    <div id="comment-blueprint" class="m1">
        <a href="/posts/delete/44"
           class="close text-secondary delete-comment-modal-trigger"
           onclick="return false;" data-target="#deleteCommentModal"
           data-comment-id="44">×</a>
        <div class="d-flex flex-row">
            <div class="pl-2 pt-2">
                <div class="profile-picture-comment-container">
                    <?= $this->Html->image('/files/ProfilePictures/image_name/default_profile_picture.png',
                        array(
                            'alt' => 'Profile-picture',
                            'class' => 'img-fluid comment-user-picture',
                            'id' => 'post-user-profile-picture'
                        )); ?>
                </div>
            </div>
            <div class="p-2 w-100">
                <div class="row pl-2">
                    <div class="col-6">
                        <a href="/users/view/0" class="font-weight-bold text-primary comment-user-full-name">default</a><br>
                        <a href="#" class="text-success comment-user-email">
                            <small>fix22@gmail.com </small></a>
                    </div>
                    <div class="col-12 pt-2 my-2">
					<span id="comment-content" class="text-black">
					something
					</span>
                        <br>
                        <small class="text-secondary comment-created">
                            (Friday October 04, 10:36 )
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
