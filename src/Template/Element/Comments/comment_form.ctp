<div class="d-none">
    <?php echo $this->Form->create('Comment', array(
        'url' => array(
            'controller' => 'comments',
            'action' => 'add'
        ),
        'id' => 'comment-form'
    ));
    ?>
    <?php
    echo $this->Form->control('content', array('id' => 'comment-form-content', 'type' => 'textArea')); ?>
    <?php
    $this->Form->unlockField('post_id');
    echo $this->Form->control('post_id', array(
        'value' => 7,
        'type' => 'hidden',
        'id' => 'comment-form-post-id'
    )); ?>
    <?php echo $this->Form->button(__('save'), array('class' => 'btn btn-success')); ?>
    <?php echo $this->Form->end(); ?>

    <?php
    $this->append('js_block', $this->Html->script('comment.js'));
    ?>
</div>
