<?php
/**
 * @var $title string
 * @var $dropdownUrl array
 */
?>
<div class="dropdown">
    <a class="dropdown-toggle nav-link pointer-on-hover text-uppercase" data-toggle="dropdown" aria-expanded="false">
        <?= $title ?>
    </a>
    <div class="dropdown-menu">
        <?php foreach ($dropdownUrl as $url): ?>
            <?php
            echo $this->Html->link(
                $url['title'],
                $url['url'],
                array('class' => 'dropdown-item')
            );
            ?>
        <?php endforeach; ?>
    </div>
</div>
