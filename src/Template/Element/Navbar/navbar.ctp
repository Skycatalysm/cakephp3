<nav class="bg-primary">
    <nav class="navbar border border-0 navbar-expand-lg navbar-dark container">
        <?php
        echo $this->Html->link(__('MICROBLOG'),
            array('controller' => 'posts', 'action' => 'feed'),
            array('class' => 'navbar-brand')
        )
        ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav w-100 mr-auto">
                <?php if ($this->Session->read('Auth.User')) : ?>
                    <?php echo $this->Form->create(null, array(
                        'class' => 'w-100 my-2 my-md-0 form-search-checker',
                        'type' => 'get',
                        'url' => array('controller' => 'users', 'action' => 'search')
                    )); ?>
                    <div>
                        <div class="input-group">
                            <input value="<?= h($this->request->getQuery('q')); ?>" type="text"
                                   class="form-control form-required"
                                   name="q" placeholder="Find people...">
                            <div class="input-group-append">
                                <button class="btn btn-primary border" type="submit">search</button>
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                <?php endif ?>
            </ul>
            <div class="w-50 d-flex justify-content-lg-end">
                <?php
                $navItem = array();
                if ($this->Session->read('Auth.User')) {
                    $dropdownUrl[] = array(
                        'title' => 'Profile',
                        'url' => array('controller' => 'users', 'action' => 'view', $this->Session->read('Auth.User.id'))
                    );
                    $dropdownUrl[] = array(
                        'title' => 'Update Information',
                        'url' => array('controller' => 'users', 'action' => 'update', $this->Session->read('Auth.User.id'))
                    );
                    $dropdownUrl[] = array(
                        'title' => 'Update Profile Picture',
                        'url' => array('controller' => 'profilePictures', 'action' => 'update')
                    );
                    $dropdownUrl[] = array(
                        'title' => 'Change Password',
                        'url' => array('controller' => 'users', 'action' => 'changePassword', $this->Session->read('Auth.User.id'))
                    );

                    $navItem[] = $this->Html->link(
                        'NEWS FEED',
                        array('controller' => 'posts', 'action' => 'feed'),
                        array('class' => 'nav-link')
                    );

                    $navItem[] = $this->element(
                        'Navbar/navbar-dropdown',
                        ['title' => h($this->Text->truncate(
                            $this->Session->read('Auth.User.username'),
                            20,
                            [
                                'ellipsis' => '...',
                                'exact' => false
                            ]
                        )), 'dropdownUrl' => $dropdownUrl]);

                    $navItem[] = $this->Html->link(
                        'LOGOUT',
                        array('controller' => 'users', 'action' => 'logout'),
                        array('class' => 'nav-link')
                    );
                } else {
                    $navItem[] = $this->Html->link(
                        'LOGIN',
                        array('controller' => 'users', 'action' => 'login'),
                        array('class' => 'nav-link')
                    );

                    $navItem[] = $this->Html->link(
                        'REGISTER',
                        array('controller' => 'users', 'action' => 'register'),
                        array('class' => 'nav-link')
                    );
                }
                ?>
                <?= $this->element('Navbar/navbar-nav', ['navItem' => $navItem]) ?>
            </div>
        </div>
    </nav>
</nav>
