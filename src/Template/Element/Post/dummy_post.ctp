<div class="dummy-post">
    <div class="row my-3">
        <div class="col-12">
            <div class="p-3 rounded border border-secondary shadow">
                <div class="d-flex flex-row">
                    <div class="pl-2">
                        <div class="profile-picture-post-container border border-light fading-bg">
                        </div>
                    </div>
                    <div class="pl-2 w-100">
                        <div class="row">
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="card py-3 fading-bg border border-secondary">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 text-right">
                            </div>
                            <div class="col-12 py-3">
                                <div class="fading-bg py-5 rounded"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
