<?php
/**
 * @var $post \App\Model\Entity\Post
 */
if (isset($postEntity)) {
    $post = $postEntity;
}
?>
<?= $this->Form->create($post, array('class' => 'form-checker', 'type' => 'file')) ?>
<div class="row justify-content-center">
    <div class="col-12">
        <div class="p-3 rounded border border-secondary shadow">
            <div class="mb-2">
                <b><?= _('Create Post') ?></b>
            </div>
            <?php
            $formName = 'content';
            $classList = 'form-control form-required';
            $errorMessage = array();
            $type = 'textArea';
            $label = false;
            $rows = 3;
            $maxlength = 140;
            if ($post->getError($formName)) {
                foreach ($post->getError($formName) as $item => $value) {
                    $errorMessage[] = '<small class="text-danger">' . $value . '</small>';
                }
                $classList = $classList . ' border border-danger';
            }
            $placeHolder = 'Say something...';
            echo $this->element('Forms/form_group', array(
                'formName' => $formName,
                'classList' => $classList,
                'placeHolder' => $placeHolder,
                'errorMessage' => $errorMessage,
                'type' => $type,
                'label' => $label,
                'rows' => $rows,
                'maxlength' => $maxlength
            ));
            ?>
            <div id="dropify-invi" class="d-none">
                <?php
                $formName = 'image_name';
                $classList = 'form-control mt-0 dropify';
                $errorMessage = array();
                $type = 'file';
                $label = false;
                if ($post->getError($formName)) {
                    foreach ($post->getError($formName) as $item => $value) {
                        $errorMessage[] = '<small class="text-danger">' . $value . '</small>';
                    }
                    $classList = $classList . ' border border-danger';
                }
                $placeHolder = null;
                ?>
                <?php
                echo $this->Form->control($formName, array(
                    'class' => $classList,
                    'placeholder' => $placeHolder,
                    'required' => false,
                    'error' => false,
                    'type' => $type,
                    'label' => $label,
                    'data-max-file-size' => '2M',
                    'data-allowed-file-extensions' => 'png jpg jpeg PNG JPG JPEG'
                ));
                ?>
                <div class="form-messages">
                    <?php
                    foreach ($errorMessage as $msg) {
                        echo $msg;
                    }
                    ?>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col">
                    <a id="dropify-click" href="#" class="text-success ml-1" onclick="$(this).blur(); return false;">
                        <small>add image</small>
                    </a>
                </div>
                <div class="col text-right">
                    <?= $this->Form->button(__('post'), array('class' => 'btn px-3 btn-outline-success')) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>

