<div class="d-flex flex-row">
    <div class="pl-2">
        <div class="profile-picture-post-container border border-light">
            <?= $this->Html->image('/files/ProfilePictures/image_name/default_profile_picture.png',
                array(
                    'alt' => 'Profile-picture',
                    'class' => 'img-fluid comment-user-picture',
                    'id' => 'post-user-profile-picture'
                )); ?>
        </div>
    </div>
    <div class="pl-2 w-100">
        <div class="row">
            <div class="col-6">
                <span class="font-weight-bold">
                    <a class="text-primary flexible-text" id="post-user-full-name" href="#">
                        Full Name
                    </a></span><br>
                <small class="text-success"><a id="post-user-email" href="#">Email Address</a></small>
            </div>
            <div class="col-6 text-right">
                <a onclick="return false;" href="#" id="post-edit-button"
                   class="mr-2 text-success edit-post-modal-trigger"
                   data-target="#editPostModal" data-post-id="167">edit</a>
                <a href="/posts/delete/167" id="post-delete-button"
                   class="close text-secondary delete-post-modal-trigger"
                   onclick="return false;" data-target="#deletePostModal" data-post-id="#post-id167">×</a>
            </div>
            <div class="col-12 py-3">
                <?= $this->element('Post/post_content') ?>
            </div>
            <div class="col-12 others-block">
                <small class="text-info" id="post-like-count"></small>
                <div class="d-flex flex-row mb-3">
                    <div>
                        <button id="post-like-button"
                                class="btn btn-sm px-3 btn-outline-success ajaxLike"
                                data-liked-id="163">
                            like
                        </button>
                    </div>
                    <div>
                        <!-- Button trigger modal -->
                        <button id="post-share-button" type="button"
                                class="btn btn-sm px-3 btn-outline-success ml-2 share-post-modal-trigger"
                                data-target="#sharePostModal" data-post-id="163">
                            share
                        </button>
                    </div>
                </div>
                <?= $this->element('Comments/comment_dummy_form') ?>
            </div>
        </div>
    </div>
</div>
