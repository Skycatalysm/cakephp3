<div id="post-blueprint" class="d-none">
    <div class="row my-3">
        <div class="col-12">
            <div class="p-3 rounded border border-secondary shadow">
                <?= $this->element('Post/post_body') ?>
            </div>
        </div>
    </div>
</div>
