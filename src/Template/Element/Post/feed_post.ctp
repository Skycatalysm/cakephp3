<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $post
 */
?>
<div id="post<?= h($post['id']); ?>" data-aos="fade-in" class="aos-init aos-animate">
    <div
        class="row my-3" <?php echo((empty($post['shared_post_id'])) ? 'shared-post-id="' . h($post['id']) . '"' : ''); ?>>
        <div class="col-12">
            <div class="p-3 rounded border border-secondary shadow">
                <div class="d-flex flex-row">
                    <div class="pl-2">
                        <div class="profile-picture-post-container border border-light">
                            <?= $this->Html->image($post['user']['profile_picture'],
                                array(
                                    'alt' => 'Profile-picture',
                                    'class' => 'img-fluid comment-user-picture',
                                    'id' => 'post-user-profile-picture'
                                )); ?>
                        </div>
                    </div>
                    <div class="pl-2 w-100">
                        <div class="row">
                            <div class="col-6">
                                <span class="font-weight-bold"><?php
                                    echo $this->Html->link(h($post['user']['full_name']),
                                        array('controller' => 'users', 'action' => 'view', $post['user']['id']),
                                        array('class' => 'text-primary flexible-text')
                                    )
                                    ?></span>
                                <br>
                                <small class="text-success flexible-text">
                                    <?php if ((int)$this->Session->read('Auth.User.id') !== $post['user']['id']): ?>
                                        <a href="mailto:<?= h($post['user']['email']) ?>">
                                            <?= h($post['user']['email']) ?>
                                        </a>
                                    <?php else: ?>
                                        <a class="email-trivia" href="#" onclick="return false;">
                                            <?= h($post['user']['email']) ?>
                                        </a>
                                    <?php endif; ?>
                                </small>
                            </div>
                            <?php if ((int)$this->Session->read('Auth.User.id') === $post['user']['id']): ?>
                                <div class="col-6 text-right">
                                    <a onclick="return false;" href="#"
                                       class="mr-2 text-success edit-post-modal-trigger"
                                       data-target="#editPostModal" data-post-id="<?= h($post['id']) ?>">edit</a>
                                    <?php
                                    echo $this->Html->link('×',
                                        array('controller' => 'posts', 'action' => 'delete', $post['id']),
                                        array(
                                            'class' => 'close text-secondary delete-post-modal-trigger',
                                            'onclick' => 'return false;',
                                            'data-target' => '#deletePostModal',
                                            'data-post-id' => $post['id']
                                        )
                                    )
                                    ?>
                                </div>
                            <?php endif; ?>
                            <div class="col-12 py-3">
                                <span class="post-content"
                                      id="post-content-<?= h($post['id']) ?>"><?= h($post['content']); ?></span>
                                <?php if (!empty($post['image_url'])): ?>
                                    <div class="d-flex justify-content-center">
                                        <div class="posted-picture">
                                            <?php
                                            echo $this->Html->image(
                                                $post['image_url'], array(
                                                    'alt' => 'Post-picture',
                                                    'class' => 'img-fluid')
                                            );
                                            ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <br>
                                <small class="moment-ago text-secondary invisible"><?= h($post['created']); ?></small>
                                <?php if (!empty($post['shared_post_id'])): ?>
                                    <?= $this->element('Post/feed_post_shared', compact('post')) ?>
                                <?php endif; ?>
                            </div>
                            <div class="col-12 others-block">
                                <small class="text-info" id="post-like-count<?= h($post['id']) ?>">
                                    <?php
                                    if (count($post['likes']) === 1) {
                                        echo count($post['likes']) . ' like';
                                    } elseif (count($post['likes']) > 1) {
                                        echo count($post['likes']) . ' likes';
                                    }
                                    ?>
                                </small>
                                <div class="d-flex flex-row mb-3">
                                    <div>
                                        <button class="btn btn-sm px-3 <?php
                                        echo ($post['liked']) ? 'btn-success' : 'btn-outline-success';
                                        ?> ajaxLike"
                                                data-liked-id="<?= h($post['id']) ?>">
                                            <?php
                                            if ($post['liked'] === true) {
                                                echo 'unlike';
                                            } else {
                                                echo 'like';
                                            }
                                            ?>
                                        </button>
                                    </div>
                                    <div>
                                        <?php if ((!empty($post['shared_post_id'])) && empty($post['shared_post'])): ?>
                                            <button type="button"
                                                    class="btn btn-sm px-3 btn-success ml-2" disabled="disabled">
                                                share
                                            </button>
                                        <?php else: ?>
                                            <!-- Button trigger modal -->
                                            <button type="button"
                                                    class="btn btn-sm px-3 btn-outline-success ml-2 share-post-modal-trigger"
                                                    data-target="#sharePostModal"
                                                    data-post-id="<?= $post['id'] ?>"
                                                    href="/posts/share/<?php echo((empty($post['shared_post_id'])) ? h($post['id']) : $post['shared_post_id']); ?>"
                                                    data-share-id="<?php echo((empty($post['shared_post_id'])) ? h($post['id']) : $post['shared_post_id']); ?>">
                                                share
                                            </button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="border p-3 mb-5 bg-white rounded">
                                    <div class="d-flex flex-row">
                                        <div class="m-1">
                                            <div class="profile-picture-comment-container">
                                                <?= $this->Html->image($this->Session->read('Auth.User.profile_picture'),
                                                    array(
                                                        'alt' => 'Profile-picture',
                                                        'class' => 'img-fluid comment-user-picture',
                                                        'id' => 'post-user-profile-picture'
                                                    )); ?>
                                            </div>
                                        </div>
                                        <div class="w-100 m-1">
                                            <textarea id="comment-content<?= h($post['id']); ?>" rows="2"
                                                      class="form-control mb-2"
                                                      placeholder="Say something..." maxlength="140"></textarea>
                                            <div class="mb-2">
                                                <button
                                                    class="btn btn-outline-success btn-sm ajax-comment float-right"
                                                    type="button" data-post-id="<?= h($post['id']); ?>">
                                                    submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="comments-list<?= h($post['id']); ?>">
                                        Comments: <br>
                                        <?php if (count($post['comments']) > 0): ; ?>
                                            <span data-show-post-comments-page="0"
                                                  class="ajax-comments pointer-on-hover text-info"
                                                  data-show-post-comments="<?= h($post['id']); ?>">Load comments...</span>
                                        <?php else: ?>
                                            <span data-show-post-comments-page="0" class="text-secondary"
                                                  data-show-post-comments="<?= h($post['id']); ?>"><small>No comments
                                                yet...</small></span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
