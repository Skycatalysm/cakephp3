<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $post
 */
?>
<?php if (!(empty($post['shared_post']))): ?>
    <div <?php echo(!(empty($post['shared_post_id'])) ? 'shared-post-id="' . h($post['shared_post_id']) . '"' : ''); ?>
        class="mt-2 bg-light rounded p-3">
        <div class="d-flex flex-row">
            <div class="pl-2 pt-2">
                <div class="profile-picture-post-container">
                    <?= $this->Html->image($post['shared_post']['user']['profile_picture'],
                        array(
                            'alt' => 'Profile-picture',
                            'class' => 'img-fluid comment-user-picture',
                            'id' => 'post-user-profile-picture'
                        )); ?>
                </div>
            </div>
            <div class="p-2 w-100">
                <div class="row pl-2">
                    <div class="col-6">
                        <?php
                        echo $this->Html->link(h($post['shared_post']['user']['full_name']),
                            array('controller' => 'users', 'action' => 'view', $post['shared_post']['user']['id']),
                            array('class' => 'text-primary font-weight-bold flexible-text')
                        )
                        ?><br>
                        <small class="text-success flexible-text">
                            <?php if ((int)$this->Session->read('Auth.User.id') !== $post['shared_post']['user']['id']): ?>
                                <a href="mailto:<?= h($post['shared_post']['user']['email']) ?>">
                                    <?= h($post['shared_post']['user']['email']) ?>
                                </a>
                            <?php else: ?>
                                <a href="#" class="email-trivia" onclick="return false;">
                                    <?= h($post['shared_post']['user']['email']) ?>
                                </a>
                            <?php endif; ?>
                        </small>
                    </div>
                    <div class="col-12 pt-2 my-2"><span
                            class="text-black"><?= h($post['shared_post']['content']); ?></span>
                        <?php if (!empty($post['shared_post']['image_url'])): ?>
                            <div class="d-flex justify-content-center">
                                <div class="posted-picture">
                                    <?php
                                    echo $this->Html->image(
                                        $post['shared_post']['image_url'], array(
                                            'alt' => 'Post-picture',
                                            'class' => 'img-fluid')
                                    );
                                    ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <br>
                        <small
                            class="text-secondary invisible moment-ago"><?= h($post['shared_post']['created']); ?></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
    <div <?php echo(!(empty($post['shared_post_id'])) ? 'shared-post-id="' . h($post['shared_post_id']) . '"' : ''); ?>
        class="border border-info p-4 rounded">
        <strong>
            Shared post content not available.
        </strong><br>
        This post has been deleted.
    </div>
<?php endif; ?>
