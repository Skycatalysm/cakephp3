<?php
/**
 * @var \App\Model\Entity\User $owner
 *
 */
?>
<div class="d-flex justify-content-center p-3 mb-3 rounded">
    <div class="d-flex align-items-center rounded h-100">

        <div class="profile-picture-md-container border border-light">
            <?= $this->Html->image($owner['profile_picture'],
                array('alt' => 'Profile-picture', 'class' => 'img-fluid')); ?>
        </div>
        <div class="mb-auto ml-3">
            <h5 class="text-primary font-weight-bold">
                <?php
                echo $this->Html->link($owner['truncated_full_name'],
                    array('controller' => 'followers', 'action' => 'followers', $owner['id']),
                    array('class' => 'text-primary')
                )
                ?>
            </h5>
            <h5 class="text-success">
                <a href="mailto:<?= h($owner['email']); ?>">
                    <?php echo
                    $this->Text->truncate(h($owner['email']), 30, ['ellipsis' => '...',
                        'exact' => false]); ?>
                </a>
            </h5>
        </div>
    </div>
</div>
