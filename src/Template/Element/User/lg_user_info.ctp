<?php
/**
 * @var \App\Model\Entity\User $user
 * @var \App\Model\Entity\Follower $follow
 */
?>
<div class="row">
    <div class="col-12">
    </div>
    <div class="col-6">
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="profile-picture-container border border-light">
                    <?= $this->Html->image($user['profile_picture'],
                        array('alt' => 'Profile-picture', 'class' => 'img-fluid comment-user-picture')); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="d-flex align-items-start flex-column h-100">
                    <div class="mb-auto">
                        <h3 class="flexible-text lg-user-full-name"><?= h($user['truncated_full_name']); ?></h3>
                        <h5 class="text-success">

                            <?php if (!((int)$this->Session->read('Auth.User.id') === h((int)$user['id']))): ?>
                                <a href="mailto:<?= h($user['email']); ?>">
                                    <?php echo
                                    $this->Text->truncate(h($user['email']), 30, [
                                        'ellipsis' => '...',
                                        'exact' => false
                                    ]); ?>
                                </a>
                            <?php else: ?>
                                <a href="#" class="email-trivia" onclick="return false;">
                                    <?php echo
                                    $this->Text->truncate(h($user['email']), 30, [
                                        'ellipsis' => '...',
                                        'exact' => false
                                    ]); ?>
                                </a>
                            <?php endif; ?>
                        </h5>
                    </div>
                    <div>
                        <div class="row follow-block">
                            <div class="col-6">
                                <div
                                    class="btn-group btn-group-sm align-content-center mb-3  bg-light rounded shadow-sm"
                                    role="group">
                                    <?php
                                    echo $this->Html->link(__('Followers:'),
                                        array('controller' => 'followers', 'action' => 'followers', $user['id']),
                                        array('class' => 'btn btn-primary')
                                    )
                                    ?>
                                    <div id="followers-count"
                                         class="text-primary px-2 ml-1 d-flex align-items-center"><?= $follow['followers']; ?></div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div
                                    class="btn-group btn-group-sm align-content-center mb-3  bg-light rounded shadow-sm"
                                    role="group">
                                    <?php
                                    echo $this->Html->link(__('Following:'),
                                        array('controller' => 'followers', 'action' => 'followings', $user['id']),
                                        array('class' => 'btn btn-primary', 'onclick' => '$(this).blur();')
                                    )
                                    ?>
                                    <div
                                        class="text-primary px-2 ml-1 d-flex align-items-center"><?= $follow['following']; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-6 text-right">
        <div class="mr-3 d-flex align-items-end flex-column h-100">
            <div>
                <b><?= __('Joined Microblog') ?></b>
            </div>
            <div class="mb-auto">
                <small id="data-user-joined-date" class="text-secondary invisible">
                    <?= h($user['created']); ?>
                </small>
            </div>
            <?php if (!((int)$this->Session->read('Auth.User.id') === h((int)$user['id']))): ?>
                <button data-followed-user-id="<?= h($user['id']) ?>" class="btn btn-primary px-3 ajax-follow"
                        type="submit">
                    <?php echo ($follow['status']) ? 'unfollow' : 'follow'; ?>
                </button>
            <?php endif; ?>
        </div>
    </div>
</div>
<hr data-current-user-profile="<?= $user['id']; ?>">
