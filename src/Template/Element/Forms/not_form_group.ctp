<?php
/**
 * @var string $formName should contain the name of form input
 * @var string $classList should contain the class of form input
 * @var string $placeHolder should contain the placeholder of form input
 * @var string $type should contain the input type
 */

if (!isset($type)) {
    $type = null;
}
if (!isset($label)) {
    $label = null;
}
if (!isset($rows)) {
    $rows = null;
}
if (!isset($maxlength)) {
    $maxlength = null;
}
?>
<?php
echo $this->Form->control($formName, array(
    'class' => $classList,
    'placeholder' => $placeHolder,
    'required' => false,
    'error' => false,
    'type' => $type,
    'label' => $label,
    'rows' => $rows,
    'maxlength' => $maxlength
));
?>
<div class="form-messages">
    <?php
    foreach ($errorMessage as $msg) {
        echo $msg;
    }
    ?>
</div>
