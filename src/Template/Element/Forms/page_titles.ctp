<?php
/**
 * @var $title string
 */
?>

<?php $this->assign('title', $title); ?>
<h5 class="font-weight-bold text-center"><?= $title ?></h5>
