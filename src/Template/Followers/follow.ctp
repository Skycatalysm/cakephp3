<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Follower $followers
 */
?>

<?php
echo json_encode(compact('followers'));
?>
