<?php
/**
 * @var \App\Model\Entity\Follower $followers
 * @var \App\Model\Entity\User $owner
 */
$this->assign('title', h($owner['first_name'] . '\'s Followers'));
?>
<div class="d-flex flex-row justify-content-center mb-3">
    <div class="btn-group btn-group-lg align-content-center
    bg-light p-0 m-0 rounded shadow-sm" role="group">
        <?php
        echo $this->Html->link($owner['first_name'] . '\'s Followers',
            array('controller' => 'users', 'action' => 'view', $owner['id']),
            array('class' => 'btn btn-primary', 'onclick' => '$(this).blur();')
        )
        ?>
        <div class="text-primary px-2 ml-1 d-flex align-items-center">
            <h5>(<?php echo $this->Paginator->params()['count']; ?>)</h5>
        </div>
    </div>
</div>
<?php if ($this->Paginator->params()['count'] > 0): ?>
    <div class="row justify-content-md-center">
        <?php foreach ($followers as $follower): ?>
            <?= $this->element('Followers/follow_user', compact('follower')) ?>
        <?php endforeach; ?>
    </div>
    <?= $this->element('Followers/follow') ?>
<?php else: ?>
    <div class="p-2 my-3 rounded border border-light shadow-sm">
        <div class="row">
            <div class="col-md-12 p-5 text-center">
                <h3 class="font-weight-bold text-info">Currently has no follower</h3>
                <p class="text-secondary">No one is following <?= $owner['first_name'] ?> currently. </p>
            </div>
        </div>
    </div>
<?php endif; ?>

<?= $this->element('Pagination/pagination') ?>
