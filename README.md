# CakePHP 3 Microblog
running on cakephp 3

## Installating

Run bash then cd to the root directory
```
cd cakephp3
```

Run composer update
```
composer update
```

Cd inside config folder(/cakephp3/config)
```
cd config
```

Copy, paste and rename app.default.php to app.php by running
```
cp app.default.php app.php
```

Open the new app.php file via a text editor, find then change the config for the database
 then the gmails username and  password then save and close it,

Go back to the root directory and edit phinx.yml with your config for the database, then run:
```
vendor/bin/phinx migrate -e development
```

To test, run this on the root directory:
```
bin/cake server -p 8765
```

Then visit `http://localhost:8765`