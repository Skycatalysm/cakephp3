<?php

use Phinx\Migration\AbstractMigration;

class PostsMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table posts
        $this->table('posts')
            ->addColumn('user_id', 'integer')
            ->addColumn('shared_post_id', 'integer', ['null' => true])
            ->addColumn('content', 'string', ['limit' => 140])
            ->addColumn('image_name', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime', ['null' => true])
            ->addColumn('status', 'boolean', ['default' => true])
            ->addForeignKey('user_id', 'users', ['id'])
            ->create();
    }
}
