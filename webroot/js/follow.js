$(document).ready(function () {
    $(document).delegate(".ajax-follow", 'click', function () {

        $(this).blur();
        let followedId = $(this).attr('data-followed-user-id');
        let followButton = $(this);
        followButton.html('<span class="mx-3 spinner-border-sm spinner-border"></span>');
        $('#followed_user_id').val(followedId);

        var form = $('#follow-form');

        var ajaxTPCalls = form.serializeArray();
        if (followButton.attr('data-done') === 'false') {
            return false;
        }
        followButton.attr('data-done', 'false');
        $.ajax({
            type: form.attr('method'),
            async: true,
            url: form.attr('action'),
            data: ajaxTPCalls,
            dataType: "json",
            cache: false,
            success: function (data) {
                if (data['followers']['status'] === 'failed') {
                    location.reload();
                    return;
                }
                if (data['followers']['status'] === true) {
                    followButton.html('unfollow');
                } else {
                    followButton.html('follow');
                }
                $('#followers-count').text(data['followers']['followers']);
                followButton.removeAttr('data-done');
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {
                    $("body").html(jqXHR.responseText);
                }
                followButton.removeAttr('data-done');
                followButton.html('follow');
            }
        });

    });
});
