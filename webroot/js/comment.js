$(document).ready(function () {
    $(document).delegate(".ajax-comment", 'click', function () {
        $(this).blur();
        let commentedButton = $(this);
        let commentedId = $(this).attr('data-post-id');
        let commentedTextArea = $('#comment-content' + commentedId);
        $('#comment-form-post-id').val(commentedId);
        let commentedContent = commentedTextArea.val();
        $('#comment-form-content').val(commentedContent);
        let form = $('#comment-form');

        //loaders
        commentedTextArea.removeClass('border border-danger is-valid');
        let $parentDiv = commentedTextArea.parent().first();
        $('.commenting-result-message', $parentDiv).remove();
        //bootstrap loader
        commentedButton
            .before(
                '<span class="spinner-border text-secondary spinner-border-sm commenting-result-message"></span>'
            );

        let ajaxTPCalls = form.serializeArray();
        if (commentedButton.attr('data-done') === 'false') {
            return false;
        }
        commentedButton.attr('data-done', 'false');
        $.ajax({
            type: form.attr('method'),
            async: true,
            url: form.attr('action'),
            data: ajaxTPCalls,
            dataType: "json",
            cache: false,
            success: function (data) {
                $('.commenting-result-message', $parentDiv).remove();
                if (data['comment']['status'] === false) {
                    commentedTextArea.removeClass('is-valid');
                    commentedTextArea.addClass('border border-danger');
                    for (let errors in data['comment']['errors']) {
                        commentedButton.before('' +
                            '<small class="text-danger m-0 commenting-result-message">'
                            + data['comment']['errors'][errors] +
                            '</small>');
                    }
                }
                if (data['comment']['status'] === true) {
                    commentedTextArea.addClass('is-valid');
                    commentedTextArea.removeClass('border border-danger');
                    commentedTextArea.val('');
                    commentedButton.before('' +
                        '<small class="text-success m-0 commenting-result-message">'
                        + 'Success!' +
                        '</small>');

                    let dataCommentLoader = $("[data-show-post-comments='" + commentedId + "']");
                    dataCommentLoader.text('Load comments...');
                    dataCommentLoader.addClass('ajax-comments pointer-on-hover text-info');
                }
                commentedButton.removeAttr('data-done');
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {
                    $("body").html(jqXHR.responseText);
                }
                commentedButton.removeAttr('data-done');
            }
        });

    });
});
