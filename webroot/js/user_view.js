$(document).ready(function () {
    loadPosts();
    $('#data-user-joined-date').text(moment($('#data-user-joined-date').text().trim()).fromNow());
    $('#data-user-joined-date').removeClass('invisible');
});


//For data to string vs xss attacks
function dataToString(data) {
    data = data
        .replace(/</g, '<&#x200B;')
        .replace(/>/g, '&#x200B;>')
        .replace(/\r\n|\r|\n/g, "</br>");
    return data;
}

//For loading post informations
function loadPosts() {
    const userId = $('hr').attr('data-current-user-profile');
    let $postBlock = $('#post-block');
    let $postBlockPage = $postBlock.attr('data-post-page');
    if ($postBlockPage === 'loading') {
        return;
    }
    let $recentPostId = '';
    if ($postBlockPage !== 'null') {
        $recentPostId = '/' + $postBlockPage;
    }
    try {
        $postBlock.attr('data-post-page', 'loading');
        $.get('/posts/user-post/' + userId + $recentPostId, function (data, status) {
            let arr = $.parseJSON(data);

            arr = arr['posts'];
            for (let i = 0; i < arr.length; i++) {
                //clone the div
                let $bluePrint = $('#post-blueprint').clone();

                //set var for post user owner
                let postUser = arr[i]['user'];

                //set var for current post
                let currentPost = arr[i];
                var hasNext = currentPost['hasNext'];

                //post user full name
                let $userNameBlock = $("#post-user-full-name", $bluePrint);
                $userNameBlock.text(postUser['full_name']);
                $userNameBlock.attr('href', '/users/view/' + postUser['id']);
                $userNameBlock.removeAttr('id');

                //post user email
                let $userEmailBlock = $("#post-user-email", $bluePrint);
                $userEmailBlock.text(postUser['email']);
                $userEmailBlock.attr('href', 'mailto:' + postUser['email']);
                if (currentPost['Auth'] === true) {
                    $userEmailBlock.attr('href', '#');
                    $userEmailBlock.attr('onclick', 'return false');
                    $userEmailBlock.addClass('email-trivia');
                }
                $userEmailBlock.removeAttr('id');

                //post user profile picture
                let $userProfilePictureBlock = $("#post-user-profile-picture", $bluePrint);
                $userProfilePictureBlock.attr('src', postUser['profile_picture']);
                $userProfilePictureBlock.removeAttr('id');


                //post edit button
                let $postEditButton = $("#post-edit-button", $bluePrint);
                $postEditButton.attr('data-post-id', currentPost['id']);
                $postEditButton.removeAttr('id');
                if (currentPost['Auth'] === false) {
                    $postEditButton.remove();
                }

                //post delete button
                let $postDeleteButton = $("#post-delete-button", $bluePrint);
                $postDeleteButton.attr('href', '/posts/delete/' + currentPost['id']);
                $postDeleteButton.attr('data-post-id', currentPost['id']);
                $postDeleteButton.removeAttr('id');
                if (currentPost['Auth'] === false) {
                    $postDeleteButton.remove();
                }

                //post share button
                let $postShareButton = $("#post-share-button", $bluePrint);
                $postShareButton.attr('href', '/posts/share/' + currentPost['id']);
                $postShareButton.attr('data-post-id', currentPost['id']);
                $postShareButton.attr('data-share-id', currentPost['id']);
                $postShareButton.removeAttr('id');

                //post like button
                let $postLikeButton = $("#post-like-button", $bluePrint);
                if (currentPost['React'] === true) {
                    $postLikeButton.removeClass('btn-outline-success');
                    $postLikeButton.addClass('btn-success');
                    $postLikeButton.text('unlike');
                }
                $postLikeButton.attr('data-liked-id', currentPost['id']);
                $postLikeButton.removeAttr('id');

                //post like count
                let $postLikeCount = $("#post-like-count", $bluePrint);
                $postLikeCount.text(currentPost['ReactCount']);
                $postLikeCount.attr('id', 'post-like-count' + currentPost['id']);

                //post commentlet
                let $postCommentButton = $("#post-comment-button", $bluePrint);
                let $postCommentContent = $("#comment-input", $bluePrint);
                let clonedCommentsList = $("#comments-list").clone();
                let clonedCommentShowButton = $("#comments-show-button", clonedCommentsList);
                clonedCommentShowButton.attr('data-show-post-comments', currentPost['id']);
                let $commentCounter = currentPost['comments'];
                if ($commentCounter.length === 0) {
                    clonedCommentShowButton.attr('class', 'text-secondary');
                    clonedCommentShowButton.html('<small>No comments yet...</small>');
                }
                clonedCommentShowButton.removeAttr('id');
                clonedCommentsList.attr('id', 'comments-list' + currentPost['id']);
                $postCommentContent.parent().first().parent().first().after(clonedCommentsList);
                $postCommentButton.attr('data-post-id', currentPost['id']);
                $postCommentContent.attr('id', 'comment-content' + currentPost['id']);
                $postCommentButton.removeAttr('id');

                //post post content
                let $postContent = $("#post-content", $bluePrint);
                $postContent.html(currentPost['content']
                    .replace(/</g, '<&#x200B;')
                    .replace(/>/g, '&#x200B;>')
                    .replace(/\r\n|\r|\n/g, '</br>'));
                $postContent.attr('id', 'post-content-' + currentPost['id']);
                if (currentPost['image_url'] !== null) {
                    $postContent.after(
                        '<div class="d-flex justify-content-center"> ' +
                        '<div class="posted-picture">' +
                        '<img class="img-fluid" src="' + currentPost['image_url'] + '" alt="post-picture">' +
                        '</div>' +
                        '</div>'
                    );
                }

                //post post created
                let $postCreated = $("#post-created", $bluePrint);
                $postCreated.text(moment(currentPost['created']).fromNow());
                $postCreated.removeAttr('id');

                //post shared post variable
                let postShared = currentPost['shared_post'];
                let postSharedDiv = '';
                $('div:first', $bluePrint).attr('shared-post-id', currentPost['id']);
                if (postShared === null && currentPost['shared_post_id'] !== null) {
                    $postShareButton.removeAttr('href');
                    $postShareButton.removeAttr('data-post-id');
                    $postShareButton.removeAttr('data-share-id');
                    $postShareButton.removeAttr('data-target');
                    $postShareButton.removeClass('share-post-modal-trigger');
                    $postShareButton.removeClass('btn-outline-success');
                    $postShareButton.addClass('btn-success');
                    $postShareButton.attr('disabled', true);
                    $('div:first', $bluePrint).removeAttr('shared-post-id');
                    postSharedDiv = '' +
                        '<div shared-post-id="' + currentPost['shared_post_id'] + '" class="border border-info p-4 rounded">\n' +
                        '<strong>\n' +
                        'Shared post content not available.\n' +
                        '</strong><br>\n' +
                        'This post has been deleted.\n' +
                        '</div>' +
                        '';
                    $postCreated.after(postSharedDiv);
                }
                if (postShared !== null) {
                    let postSharedUser = postShared['user'];
                    let sharedMailTo = 'mailto:' + postSharedUser['email'];
                    if (currentPost['shared_auth'] === true) {
                        sharedMailTo = '#" onclick="return false" class="email-trivia'
                    }
                    $postShareButton.attr('data-share-id', postShared['id']);
                    $('div:first', $bluePrint).removeAttr('shared-post-id');
                    let postSharedImg = '';
                    if (postShared['image_url'] !== null) {
                        postSharedImg =
                            '<div class="d-flex justify-content-center"> ' +
                            '<div class="posted-picture">' +
                            '<img class="img-fluid" src="' + postShared['image_url'] + '" alt="post-picture">' +
                            '</div>' +
                            '</div>';
                    }
                    postSharedDiv = '' +
                        '<div shared-post-id="' + postShared['id'] + '" class="mt-2 bg-light rounded p-3">\n' +
                        '<div class="d-flex flex-row">\n' +
                        '<div class="pl-2 pt-2">\n' +
                        '<div class="profile-picture-post-container">\n' +
                        '<img src="' + postSharedUser['profile_picture'] + '" alt="Profile-picture"></div>\n' +
                        '</div>\n' +
                        '<div class="p-2 w-100">\n' +
                        '<div class="row pl-2">\n' +
                        '<div class="col-6">\n' +
                        '<a href="/users/view/' + postSharedUser['id'] + '" class="font-weight-bold text-primary flexible-text">' + dataToString(postSharedUser['full_name']) + '</a>\<br>\n' +
                        '<small class="text-success flexible-text"> <a href="' + sharedMailTo + '">\n' +
                        postSharedUser['email']
                        + '</a></small>\n' +
                        '</div>\n' +
                        '<div class="col-12 pt-2 my-2">\n' +
                        '<span class="text-black">\n'
                        +
                        dataToString(postShared['content'])
                        +
                        '</span>\n' + postSharedImg +
                        '<br>\n' +
                        '<small class="text-secondary">\n' +
                        moment(postShared['created']).fromNow()
                        +
                        '</small>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</div>';
                    $postCreated.after(postSharedDiv);
                }

                // post post id
                $($bluePrint).attr('id', 'post' + currentPost['id']);
                $($bluePrint).attr('data-aos', 'fade-in');
                $($bluePrint).removeAttr('class');
                $postBlock.append($bluePrint);
                var postID = currentPost['id'];
            }
            if (hasNext === false) {
                let nullShow = '<div class="mt-5">Nothing more to show...</div>';
                $('#end-post-block').html(nullShow);
                return false;
            }
            if (postID === undefined) {
                let nullShow = '<div class="mt-5">Nothing more to show...</div>';
                $('#end-post-block').html(nullShow);
                return false;
            }
            $postBlock.attr('data-post-page', postID);
            if (isElementInViewport($('#end-post-block'))) {
                loadPosts();
            }
        });
    } catch (err) {
        return false;
    }
}


//For load on scroll
function isElementInViewport(el) {

    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}

function onVisibilityChange(el, callback) {
    var old_visible;
    return function () {
        var visible = isElementInViewport(el);
        if (visible != old_visible) {
            old_visible = visible;
            if (typeof callback == 'function') {
                callback();
            }
        }
    }
}

var handler = onVisibilityChange($('#end-post-block'), function () {
    loadPosts();
});

//jQuery
$(window).on('DOMContentLoaded load resize scroll', handler);
