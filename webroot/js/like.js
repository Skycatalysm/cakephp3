$(document).ready(function () {
    $(document).delegate(".ajaxLike", 'click', function () {

        $(this).blur();
        let likedId = $(this).attr('data-liked-id');
        let likedButton = $(this);
        likedButton.html('<span class="spinner-border-sm spinner-border"></span>');
        $('#data-like-id-target').val(likedId);

        var form = $('#like-form');

        var ajaxTPCalls = form.serializeArray();
        if (likedButton.attr('data-done') === 'false') {
            return false;
        }
        likedButton.attr('data-done', 'false');
        $.ajax({
            type: form.attr('method'),
            async: true,
            url: form.attr('action'),
            data: ajaxTPCalls,
            dataType: "json",
            cache: false,
            success: function (data) {
                $('#post-like-count' + likedId).text(data['likes']['count']);
                if (data['likes']['status'] === true) {
                    likedButton.removeClass('btn-outline-success');
                    likedButton.addClass('btn-success');
                    likedButton.text('unlike');
                } else {
                    likedButton.removeClass('btn-success');
                    likedButton.addClass('btn-outline-success');
                    likedButton.text('like');
                }
                likedButton.removeAttr('data-done');
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {
                    $("body").html(jqXHR.responseText);
                }
                likedButton.removeAttr('data-done');
            }
        });

    });
});
