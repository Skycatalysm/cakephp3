$(document).ready(function () {
    $('.moment-ago').each(function () {
        $(this).text(moment($(this).text()).fromNow());
        $(this).removeClass('invisible');
    });
});
