$(document).ready(function () {
//For the preview of post on edit
    $('#data-post-content-target').on('input', function () {
        let text = $(this).val();
        $('#post-content-input').html(text
            .replace(/</g, '<&#x200B;')
            .replace(/>/g, '&#x200B;>')
            .replace(/\r\n|\r|\n/g, "</br>"));
    });

//For edit post modal
    $(document).delegate(".edit-post-modal-trigger", 'click', function () {
        let modalToToggle = $(this).attr('data-target');
        let currentPostId = $(this).attr('data-post-id');
        let currentPost = $('#post' + currentPostId).clone();
        currentPost.removeAttr('id');
        let currentPostContent = $('#post-content-' + currentPostId).clone();
        currentPostContent.html(currentPostContent.html().replace(/<br\s*[\/]?>/gi, '\n').replace(/^ +/gm, ''));
        currentPostContent = currentPostContent.text();
        $(".edit-post-modal-trigger", currentPost).remove();
        $(".delete-post-modal-trigger", currentPost).remove();
        $(".shadow", currentPost).removeClass("shadow");
        $(".others-block", currentPost).remove();
        $("#post-content-" + currentPostId, currentPost).attr('id', 'post-content-input');
        $("#data-post-content-target").val(currentPostContent.trim());
        $("#data-post-shared-content-target").html(currentPost);
        $("#data-post-id-target").val(currentPostId);
        $(modalToToggle).modal();
    });

//For delete post modal
    $(document).delegate(".delete-post-modal-trigger", 'click', function () {
        let deleteBody = $(this).attr('href');
        let modalToToggle = $(this).attr('data-target');
        let currentPostId = $(this).attr('data-post-id');
        let currentPost = $('#post' + currentPostId).clone();
        currentPost.removeAttr('id');
        $(".edit-post-modal-trigger", currentPost).remove();
        $(".delete-post-modal-trigger", currentPost).remove();
        $(".shadow", currentPost).removeClass("shadow");
        $(".others-block", currentPost).remove();
        $("#deletePostModalBody").html(currentPost);
        $("#deletePostUrl").attr('href', deleteBody)
        $(modalToToggle).modal();
    });

//For share post modal
    $(document).delegate(".share-post-modal-trigger", 'click', function () {
        let modalToToggle = $(this).attr('data-target');
        let currentPostId = $(this).attr('data-post-id');
        let currentPostSharedId = $(this).attr('data-share-id');
        let currentPost = $('#post' + currentPostId).clone();
        let currentPostShared = $('[shared-post-id=' + currentPostSharedId + ']', currentPost).clone();
        currentPost.removeAttr('id');
        $(".edit-post-modal-trigger", currentPostShared).remove();
        $(".delete-post-modal-trigger", currentPostShared).remove();
        $(".shadow", currentPostShared).removeClass("shadow");
        $(".border", currentPostShared).addClass("bg-light");
        $(".border", currentPostShared).addClass("mt-2");
        $(".border", currentPostShared).removeClass("border");
        $(".others-block", currentPostShared).remove();
        $("#share-post-content-target").html(currentPostShared);
        $("#shareIdTarget").val(currentPostSharedId);
        $(modalToToggle).modal();
    });

//For delete comment modal
    $(document).delegate('.delete-comment-modal-trigger', 'click', function () {
        let deleteBody = $(this).attr('href');
        let modalToToggle = $(this).attr('data-target');
        let currentComment = $(this).attr('data-comment-id');
        let clonedComment = $('[comment-id="' + currentComment + '"]').clone();
        $('.delete-comment-modal-trigger', clonedComment).remove();
        let currentCommentContent = $(clonedComment).html();
        $("#deleteCommentModalBody").html(currentCommentContent);
        $("#deleteCommentUrl").attr('href', deleteBody)
        $(modalToToggle).modal();
    });
});
