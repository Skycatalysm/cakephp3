$(document).ready(function () {
    //initialize AOS.js
    AOS.init({
        once: true
    });
    //initialize AOS.js
    var drEvent = $('.dropify').dropify({
        messages: {
            'error': 'File is not valid.'
        },
        tpl: {
            wrap: '<div class="dropify-wrapper rounded border"></div>',
            loader: '<div class="dropify-loader"></div>',
            message: '<div class="dropify-message"><span class="file-icon" /> <p>{{ default }}</p></div>',
            preview: '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ replace }}</p></div></div></div>',
            filename: '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
            clearButton: '<button type="button" class="dropify-clear d-none">{{ remove }}</button></div>',
            errorLine: '<p class="dropify-error">{{ error }}</p>',
            errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
        }
    });
    drEvent = drEvent.data('dropify');
    //toggle dropify add image
    $('#dropify-click').click(function () {
        if ($('#dropify-invi').hasClass('d-none')) {
            $(this).html('<small>remove image</small>');
            $('#dropify-invi').removeClass('d-none');
            return;
        }
        $('.dropify-clear').click();
        drEvent.destroy();
        drEvent.init();
        $(this).html('<small>add image</small>');
        $('#dropify-invi').addClass('d-none');

    });
    //toggle dropify add image


    //For automatically closing flash messages
    $(".alert-dismissible").delay(6000).slideUp(200, function () {
        $(this).alert('close');
    });

    //for email trivia
    $(document).delegate(".email-trivia", 'click', function () {
        $('#trivia-modal-content')
            .text('Clicking other user\'s email activates the default mail' +
                ' client on the computer for sending an e-mail to them.');
        $('#trivia-modal').modal('show');
    });

    //ajax comments request
    $(document).delegate(".ajax-comments", 'click', function () {
        let loadCommentsButton = $(this);
        let postId = (loadCommentsButton.attr('data-show-post-comments'));
        let postPageValue = (loadCommentsButton.attr('data-show-post-comments-page'));
        loadCommentsButton.attr('data-show-post-comments-page', 'loading');
        loadCommentsButton.text('');
        loadCommentsButton.removeClass('pointer-on-hover text-info');
        loadCommentsButton.addClass('spinner-border text-secondary');
        if (postPageValue === 'loading') {
            return false;
        }
        let fetchComment = '';
        if (postPageValue !== 'null') {
            fetchComment = postPageValue;
        }
        $.get('/comments/comments-ajax/' + postId + '/' + fetchComment, function (data, status) {
            let arr = $.parseJSON(data);
            arr = arr['comments'];
            if (arr === 'failed' || jQuery.isEmptyObject(arr)) {
                loadCommentsButton.attr('data-show-post-comments-page', postPageValue);
                loadCommentsButton.attr('class', 'text-secondary');
                loadCommentsButton.html('');
                return;
            }
            for (let i = 0; i < arr.length; i++) {

                //clone the div
                let $bluePrint = $('#comment-blueprint').clone();
                let currentComment = arr[i];

                //set variable contents from the ajax result
                let commentContent = currentComment['content'];
                let commentCreated = moment(currentComment['created']).fromNow();
                let commentId = currentComment['id'];


                // comment content to dom
                $("#comment-content", $bluePrint).text(commentContent);
                $(".comment-created", $bluePrint).text(commentCreated);
                $(".delete-comment-modal-trigger", $bluePrint).attr('href', '/comments/delete/' + commentId);
                $(".delete-comment-modal-trigger", $bluePrint).attr('data-comment-id', commentId);
                $("#comment-content", $bluePrint).removeAttr('id');
                if (currentComment['Auth'] === false) {
                    $(".delete-comment-modal-trigger", $bluePrint).remove();
                }

                //set variable owner contents from the ajax result
                let commentOwner = currentComment['user'];
                //
                let commentUserPicture = commentOwner['profile_picture'];
                let commentUserFullName = commentOwner['full_name'];
                let commentUserId = commentOwner['id'];
                let commentUserEmail = commentOwner['email'];

                // comment owner to dom
                $(".comment-user-picture", $bluePrint).attr('src', commentUserPicture);
                $(".comment-user-full-name", $bluePrint).attr('href', '/users/view/' + commentUserId);
                $(".comment-user-full-name", $bluePrint).text(commentUserFullName);
                $(".comment-user-full-name", $bluePrint).addClass('flexible-text');
                $(".comment-user-email", $bluePrint).addClass('flexible-text');
                if (currentComment['Auth'] === false) {
                    $(".comment-user-email", $bluePrint).attr('href', 'mailto:' + commentUserEmail);
                } else {
                    $(".comment-user-email", $bluePrint).addClass('email-trivia');
                    $(".comment-user-email", $bluePrint).attr('onclick', 'return false;');
                }
                $(".comment-user-email", $bluePrint).children(':first').text(commentUserEmail);

                //remove blueprint id redundancy
                $($bluePrint).attr('comment-id', commentId);
                $($bluePrint).attr('data-aos', 'fade');
                switch (i) {
                    case 1:
                        $($bluePrint).attr('data-aos-delay', '50');
                        break;
                    case 2:
                        $($bluePrint).attr('data-aos-delay', '100');
                        break;
                }
                $($bluePrint).removeAttr('id');

                let clonedComment = $('#comments-list' + postId);
                loadCommentsButton.before('<hr />');
                loadCommentsButton.before($bluePrint);
                var changeFetchId = commentId;
                var hasNext = currentComment['hasNext'];
            }
            loadCommentsButton.attr('data-show-post-comments-page', changeFetchId);
            if (hasNext) {
                loadCommentsButton.text('Load comments...');
                loadCommentsButton.addClass('pointer-on-hover text-info');
                loadCommentsButton.removeClass('spinner-border text-secondary');
                return false;
            }
            loadCommentsButton.attr('class', 'text-secondary');
            loadCommentsButton.html('');
        });
    });
    //end of ajax comments request

    // form
    // required
    // checker
    $('.form-checker').submit(function () {
        let $fields = $(this).find('.form-required');
        let errors = 0;
        $fields.each(function () {
                let $currentField = $(this);
                $currentField.removeClass('border border-danger');
                let formMessages = $(this).parent().parent().find('.form-messages');
                formMessages.html('');
                if ($currentField.hasClass('dropify')) {
                    $('.dropify-wrapper').removeClass('border-danger');
                    $(this)
                        .parent()
                        .parent()
                        .parent()
                        .find('.form-messages')
                        .html('');
                }
                if (this.value === '') {
                    if ($currentField.hasClass('dropify')) {
                        $('.dropify-wrapper').addClass('border-danger');
                        $(this)
                            .parent()
                            .parent()
                            .parent()
                            .find('.form-messages')
                            .html('<small class="text-danger">This field cannot be left empty</small>');
                    }
                    errors++;
                    $(this).addClass('border border-danger');
                    formMessages.html('<small class="text-danger">This field cannot be left empty</small>');
                }
            }
        );
        if (errors > 0) {
            return false;
        }
        $(this).find(':submit').attr('disabled', true);
    });
    //end of form required checker


    // form search trivia
    $('.form-search-checker').submit(function () {
        let $fields = $(this).find('.form-required');
        let errors = 0;
        $fields.each(function () {
                if (this.value === '') {
                    errors++;
                }
            }
        );
        if (errors > 0) {
            $('#trivia-modal-content')
                .html('When searching, just type some of the characters that you think matches the <span ' +
                    'class="font-weight-bold">(email, username, first name or last name)</span>' +
                    ' of the person you are looking for, then press the search button.');
            $('#trivia-modal').modal('show');
            return false;
        }
        $(this).find(':submit').attr('disabled', true);
    });
    //end of form required checker

});
